############################################################################

# Visualize number of jets in the dataset

############################################################################


# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import glob

############################################################################


# Loading Data


path1 = '/eos/user/b/backes/QT/preprocessing/' + 'L1_jfex' + '/user.backes.' #10pttruth

path2 = '.*.h5/*.h5'

test_path_1 = glob.glob(path1 + '601229' + path2)[0]
# test_path_1 = glob.glob(path1 + '601479' + path2)[0]

test_path_2 = glob.glob(path1 + '801165' + path2)[0]

test_path_3 = glob.glob(path1 + '801166' + path2)[0]

test_path_4 = glob.glob(path1 + '801167' + path2)[0]

test_path_5 = glob.glob(path1 + '801168' + path2)[0]

test_path_6 = glob.glob(path1 + '801169' + '.*.h5/*_000001.output.h5')[0]

reference = "Mathias"


print("\n=================== \n" + "  " + reference + " pt plots \n" + "=================== \n")

############################################################################

# Load test data

# Signal

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets']
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eta_sig = jets_sig['eta']
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']

# Background JZ slices

with h5py.File(test_path_2, 'r') as test_f:
    jets_jz0 = test_f['jets']
    pt_MeV_jz0 = jets_jz0['pt']
    pt_jz0 = pt_MeV_jz0/1000
    eta_jz0 = jets_jz0['eta']
    eventNumber_jz0 = jets_jz0['eventNumber']
    mc_weight_jz0 = jets_jz0['mcEventWeight']

with h5py.File(test_path_3, 'r') as test_f:
    jets_jz1 = test_f['jets']
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eta_jz1 = jets_jz1['eta']
    eventNumber_jz1 = jets_jz1['eventNumber']
    mc_weight_jz1 = jets_jz1['mcEventWeight']

with h5py.File(test_path_4, 'r') as test_f:
    jets_jz2 = test_f['jets']
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eta_jz2 = jets_jz2['eta']
    eventNumber_jz2 = jets_jz2['eventNumber']
    mc_weight_jz2 = jets_jz2['mcEventWeight']

with h5py.File(test_path_5, 'r') as test_f:
    jets_jz3 = test_f['jets']
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eta_jz3 = jets_jz3['eta']
    eventNumber_jz3 = jets_jz3['eventNumber']
    mc_weight_jz3 = jets_jz3['mcEventWeight']

with h5py.File(test_path_6, 'r') as test_f:
    jets_jz4 = test_f['jets']
    pt_MeV_jz4 = jets_jz4['pt']
    pt_jz4 = pt_MeV_jz4/1000
    eta_jz4 = jets_jz4['eta']
    eventNumber_jz4 = jets_jz4['eventNumber']
    mc_weight_jz4 = jets_jz4['mcEventWeight']


############################################################################

# MC reweighting parameters

cs_jz0 = 78580000000.0 *1e-11  
filter_jz0 = 9.736785E-01 
cs_jz1 = 93901000000.0  *1e-11 
filter_jz1 = 3.513696E-02
cs_jz2 = 2582600000.0  *1e-11
filter_jz2 = 1.006522E-02 
cs_jz3 = 28528000.0  *1e-11 
filter_jz3 = 1.190844E-02 
cs_jz4 = 280140.0  *1e-11 
filter_jz4 = 1.375122E-02


total_events_jz0 = np.unique(eventNumber_jz0).shape[0]
total_events_jz1 = np.unique(eventNumber_jz1).shape[0]
total_events_jz2 = np.unique(eventNumber_jz2).shape[0]
total_events_jz3 = np.unique(eventNumber_jz3).shape[0]
total_events_jz4 = np.unique(eventNumber_jz4).shape[0]

print("Number of JZ0 events: ", total_events_jz0)
print("Number of JZ1 events: ", total_events_jz1)
print("Number of JZ2 events: ", total_events_jz2)
print("Number of JZ3 events: ", total_events_jz3)
print("Number of JZ4 events: ", total_events_jz4)


_, unique_events_jz0, counts_jz0 = np.unique(eventNumber_jz0, return_index=True, return_counts=True)
total_events_jz0 = np.sum(mc_weight_jz0[unique_events_jz0])
_, unique_events_jz1, counts_jz1 = np.unique(eventNumber_jz1, return_index=True, return_counts=True)
total_events_jz1 = np.sum(mc_weight_jz1[unique_events_jz1])
_, unique_events_jz2, counts_jz2 = np.unique(eventNumber_jz2, return_index=True, return_counts=True)
total_events_jz2 = np.sum(mc_weight_jz2[unique_events_jz2])
_, unique_events_jz3, counts_jz3 = np.unique(eventNumber_jz3, return_index=True, return_counts=True)
total_events_jz3 = np.sum(mc_weight_jz3[unique_events_jz3])
_, unique_events_jz4, counts_jz4 = np.unique(eventNumber_jz4, return_index=True, return_counts=True)
total_events_jz4 = np.sum(mc_weight_jz4[unique_events_jz4])


weight_jz0 = cs_jz0 * filter_jz0 / 7290*50000
weight_jz1 = cs_jz1 * filter_jz1 / 1857*50000
weight_jz2 = cs_jz2 * filter_jz2 / 18.47*50000
weight_jz3 = cs_jz3 * filter_jz3 / 0.072*50000
weight_jz4 = cs_jz4 * filter_jz4 / 0.002052*50000


############################################################################

bin_min = -0.5
bin_max = 10.5
bin_number = 11

jets_jz0, bins = np.histogram(counts_jz0, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz0[unique_events_jz0]*weight_jz0)
jets_jz1, bins = np.histogram(counts_jz1, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz1[unique_events_jz1]*weight_jz1)
jets_jz2, bins = np.histogram(counts_jz2, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz2[unique_events_jz2]*weight_jz2)
jets_jz3, bins = np.histogram(counts_jz3, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz3[unique_events_jz3]*weight_jz3)
# jets_jz4, bins = np.histogram(counts_jz4, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz4[unique_events_jz4]*weight_jz4)

plt.figure(figsize=(12,8))
bottom = np.zeros_like(bins[1:], dtype=float)
jet_label = ["JZ3","JZ2","JZ1","JZ0"]
jet_color = ["r","g","orange","blue"]
for i, jet_JZ in enumerate([jets_jz3,jets_jz2,jets_jz1,jets_jz0]):
    plt.bar((bins[1:]+bins[:-1])/2, jet_JZ, bottom=bottom, width =(bin_max-bin_min)/bin_number, label=jet_label[i], color=jet_color[i])
    bottom += jet_JZ
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("Number of jets", fontsize=20)
plt.ylabel("Number of Events", fontsize=20)
plt.ylim (1e-6,1e8)
plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig("L1_jet_number.png", format='png', bbox_inches='tight')
plt.close()

############################################################################
print("Plotting successful.")
