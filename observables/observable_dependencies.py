########################################################################################################################################################

# Some older plotting routines to evaluate network performance on a jet-by-jet basis

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt
from matplotlib import colors
import mplhep as hep
hep.style.use(hep.style.ATLAS)

############################################################################

# DipZ or HitZ


# networks = {
#     "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240126-T175203/ckpts/mathias_ttbar_test.h5" 
# }
# test_path = '/eos/user/b/backes/QT/preprocessing/Dipz_train/pp_output_test_ttbar.h5'

# reference = "Dipz"


networks = {
    "Hitz_ttbar" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_ttbar_20240129-T115654/ckpts/mathias_ttbar_test.h5" 
}
test_path = '/eos/user/b/backes/QT/preprocessing/Hitz_train/pp_output_test_ttbar.h5'

reference = "Hitz"


############################################################################

# Booleans to make plots

basic_plots = 1
Single_Jet_Plots = 1
Detailed_observable_plots = 0
Common_efficiencies = 0
Sigma_efficiencies = 1
Required_sigma_max_plots = 0
Required_sigma_plots = 0
sig_over_z = 0
diff_over_observable = 0


############################################################################


# Load test data

#test_path = '/eos/user/b/backes/QT/preprocessing/' + reference + '_output/pp_output_test_ttbar.h5'   #tbd
num_jets = 35000

logger.info("Load data")
with h5py.File(test_path, 'r') as test_f:
    jets = test_f['jets'][:num_jets]
    jet_z = jets['TruthJetPVz']
    pt_MeV = jets['pt']
    pt = pt_MeV/1000
    eta = jets['eta']
    #print(test_f.keys())
    #print(jets.dtype.names)

for key, val in networks.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        # print(jets.dtype.names)
        jet_z_pred = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))
        jet_z_hitz = np.append(jet_z_pred, jet_z_stddev, axis=1)


############################################################################


# General plot

if basic_plots:

    jet_z_plot = HistogramPlot(
        bins=np.linspace(-200, 200, 41),
        xlabel="$z$ Position",
        ylabel="Normalised number of jets",
        figsize=(6, 4.5),
    )
    jet_z_plot.add(
        Histogram(
            jet_z.flatten(), label="True"
        )
    )
    jet_z_plot.add(
            Histogram(jet_z_hitz[:,0].flatten(), label=reference)
    )
    jet_z_plot.draw()
    jet_z_plot.savefig("observables_" + reference + "/jet_z.png")


############################################################################

# 2D Plot

if basic_plots:

    plt.figure(figsize=(10,8))
    h = plt.hist2d(jet_z, jet_z_hitz[:,0], bins=30, norm=colors.LogNorm())
    cb = plt.colorbar(h[3])
    cb.ax.tick_params(labelsize=20)
    plt.xlabel("Truth $z_t$ [mm]", fontsize=20)
    plt.ylabel("Predicted $z_p$ [mm]", fontsize=20)
    plt.xlim(-149.9, 149.9)
    plt.ylim(-149.9, 149.9)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    x = [-150,150]
    plt.plot(x,x,c='red')
    hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
    plt.savefig("observables_" + reference + "/2D_jet_z.png", format='png', bbox_inches='tight')
    plt.close()


############################################################################

# Difference plot

if basic_plots:

    jet_diff = jet_z-jet_z_hitz[:,0]

    jet_z_plot2 = HistogramPlot(
        bins_range=(-200, 200),
        xlabel="$z$ Position",
        ylabel="Normalised number of jets",
        figsize=(6, 4.5),
    )
    jet_z_plot2.add(
        Histogram(
            jet_z.flatten(), label="True position $z_t$"
        )
    )
    jet_z_plot2.add(
            Histogram(
                jet_diff.flatten(), label = reference + " $z_t-z_p$"
                )
    )
    jet_z_plot2.draw()
    jet_z_plot2.savefig("observables_" + reference + "/diff_jet_z.png")



# Same plot zoomed in

    jet_z_plot21 = HistogramPlot(
        bins_range=(-20, 20),
        xlabel="$z$ Position",
        ylabel="Normalised number of jets",
        figsize=(6, 4.5),
    )
    jet_z_plot21.add(
        Histogram(
            jet_z.flatten(), label="True position $z_t$"
        )
    )
    jet_z_plot21.add(
            Histogram(
                jet_diff.flatten(), label = reference + " $z_t-z_p$"
                )
    )
    jet_z_plot21.draw()
    jet_z_plot21.savefig("observables_" + reference + "/diff_jet_z_zoomed.png")


############################################################################

# Normalised plot

if basic_plots:

    jet_norm = (jet_z-jet_z_hitz[:,0])/jet_z_hitz[:,1]
    jet_norm_mean = np.mean(jet_norm)
    jet_norm_std = np.std(jet_norm)
    gaussian_reference = np.random.normal(loc=0, scale=1, size=100000)

    jet_z_plot3 = HistogramPlot(
        bins_range=(-50, 50),
        xlabel="$z$ Position",
        ylabel="Normalised number of jets",
        figsize=(6, 4.5),
        atlas_second_tag="$\\mu={:0.4f}$, $\\sigma={:0.4f}$".format(jet_norm_mean, jet_norm_std),
    )
    jet_z_plot3.add(
            Histogram(
                jet_norm.flatten(),
                label = reference + r" $(z_t-z_p)/\sigma_p$",
                )
    )
    jet_z_plot3.add(
            Histogram(
                gaussian_reference,
                label = "G($\\mu=${}, $\\sigma=${})".format(0, 1),
                )
    )
    jet_z_plot3.draw()
    jet_z_plot3.savefig("observables_" + reference + "/diff_divided.png")


# Same plot zoomed in

    jet_z_plot4 = HistogramPlot(
        bins_range=(-5, 5),
        xlabel="$z$ Position",
        ylabel="Normalised number of jets",
        figsize=(6, 4.5),
        atlas_second_tag="$\\mu={:0.4f}$, $\\sigma={:0.4f}$".format(jet_norm_mean, jet_norm_std),
    )
    jet_z_plot4.add(
            Histogram(
                jet_norm.flatten(), label = reference + r" $(z_t-z_p)/\sigma_p$"
                )
    )
    jet_z_plot4.add(
            Histogram(
                gaussian_reference,
                label = "G($\\mu=${}, $\\sigma=${})".format(0, 1),
                )
    )
    jet_z_plot4.draw()
    jet_z_plot4.savefig("observables_" + reference + "/diff_divided_zoomed.png")


############################################################################

# Single Jet plot

if Single_Jet_Plots:

    def Gauss(x, mu, sigma):
        return 1/np.sqrt(2*np.pi*sigma**2) * np.exp(-(x-mu)**2/(2*sigma**2))

    number_of_single_jets=10
    color = iter(plt.cm.rainbow(np.linspace(0, 1, number_of_single_jets)))

    plt.figure(figsize=(10,8))
    x = np.linspace(-150,150, 100000)
    print("Some single jets:")
    for i in range(number_of_single_jets):
        c = next(color)
        y = Gauss(x, jet_z_hitz[i,0], jet_z_hitz[i,1])
        plt.plot(x, y/np.max(y),
                label = "$z_t=${:0.1f}, $z_p=${:0.1f}, $\sigma_p=${:0.1f}".format(jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1],2),
                color=c)
        plt.vlines(jet_z[i], 0, 1, color=c, ls = '--')
        print("Jet {}:".format(i+1), jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1])

    plt.xlabel("$z$ [mm]", fontsize=20)
    plt.ylabel("Single jet distributions (not normalised)", fontsize=20)
    plt.ylim(0,1.8)
    plt.xlim(-119.9, 119.9)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    first_legend = plt.legend(loc="upper center", fontsize=15, ncol=2, frameon=False)
    plt.gca().add_artist(first_legend)
    line1, = plt.plot([], label="$z_t$", linestyle='--', linewidth=2, c='black')
    line2, = plt.plot([], label="$G(z_p,\\sigma_p)$", linewidth=2, c='black')
    plt.legend(handles=[line1, line2], loc=(0.35,0.6), fontsize=15, ncol=2, frameon=False)
    hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
    plt.savefig("observables_" + reference + "/Single_jets.png", format='png', bbox_inches='tight')
    plt.close()


############################################################################
    
    # Better pt dependency plot

    pt_vals=[40, 50, 90, 120, 150, 200]
    pt_range=20
    bin_min = -150
    bin_max = 150
    bin_number = 30

    for pt_0 in pt_vals:

        jet_z_cut_true = jet_z[(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]
        jet_z_cut_pred = jet_z_hitz[:,0][(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]
        ratio = (jet_z[(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]).shape[0]/jet_z.shape[0]


        N1, bin_edges = np.histogram(jet_z_cut_true, bins=bin_number, range=(bin_min,bin_max), density=True)
        N2, bin_edges = np.histogram(jet_z_cut_pred, bins=bin_number, range=(bin_min,bin_max), density=True)
        bins = (bin_edges[1:]+bin_edges[:bin_number])/2
        
        plt.figure(figsize=(10,8))
        plt.step(bins, N1, color="red", label = 'Truth', where='mid')
        plt.step(bins, N2, color="blue", label = "HitZ", where='mid')
        plt.legend(fontsize=20, frameon=False)
        plt.xlabel("$z$ [mm]", fontsize=20)
        plt.ylabel("Normalised number of jets", fontsize=20)
        plt.ylim(0,)
        plt.xlim(-149.9, 149.9)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel ="$p_T=${}$\\pm ${}GeV".format(pt_0, pt_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100), fontsize=20)
        plt.savefig("observables_" + reference + "/pt_{}_jet_z.png".format(pt_0), format='png', bbox_inches='tight')
        plt.close()
        
    eta_vals=[-2,-1,0,1,2]
    eta_range=0.5

    for eta_0 in eta_vals:

        jet_z_cut_true = jet_z[(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]
        jet_z_cut_pred = jet_z_hitz[:,0][(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]
        ratio = (jet_z[(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]).shape[0]/jet_z.shape[0]

        N1, bin_edges = np.histogram(jet_z_cut_true, bins=bin_number, range=(bin_min,bin_max), density=True)
        N2, bin_edges = np.histogram(jet_z_cut_pred, bins=bin_number, range=(bin_min,bin_max), density=True)
        bins = (bin_edges[1:]+bin_edges[:bin_number])/2
        
        plt.figure(figsize=(10,8))
        plt.step(bins, N1, color="red", label = 'Truth', where='mid')
        plt.step(bins, N2, color="blue", label = "HitZ", where='mid')
        plt.legend(fontsize=20, frameon=False)
        plt.xlabel("$z$ [mm]", fontsize=20)
        plt.ylabel("Normalised number of jets", fontsize=20)
        plt.ylim(0,)
        plt.xlim(-149.9, 149.9)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel ="$\\eta=${}$\\pm ${}".format(eta_0, eta_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100))
        plt.savefig("observables_" + reference + "/eta_{}_jet_z.png".format(eta_0), format='png', bbox_inches='tight')
        plt.close()


############################################################################

# Looking at specific z ranges

if Detailed_observable_plots:

    z_vals=[-100,-50,0,50, 100]
    z_range=20

    for z in z_vals:

        jet_z_cut_true = jet_z[(jet_z > (z-z_range)) & (jet_z < (z+z_range))]
        jet_z_cut_pred = jet_z_hitz[:,0][(jet_z > (z-z_range)) & (jet_z < (z+z_range))]
        ratio = (jet_z[(jet_z > (z-z_range)) & (jet_z < (z+z_range))]).shape[0]/jet_z.shape[0]
        
        jet_z_plot = HistogramPlot(
            bins_range=(z-50, z+50),
            xlabel="$z$ Position",
            ylabel="Normalised number of jets",
            figsize=(6, 4.5),
            atlas_second_tag="$z_t=${}$\\pm ${}".format(z,z_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100),
        )
        jet_z_plot.add(
            Histogram(
                jet_z_cut_true, label="True"
            )
        )
        jet_z_plot.add(
                Histogram(jet_z_cut_pred, label=reference)
        )
        jet_z_plot.draw()
        jet_z_plot.savefig("observables_" + reference + "/{}_jet_z.png".format(z))


############################################################################

# Looking at specific pt ranges

if Detailed_observable_plots:
    
    pt_vals=[20, 50, 90, 120, 150, 200]
    pt_range=20

    for pt_0 in pt_vals:

        jet_z_cut_true = jet_z[(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]
        jet_z_cut_pred = jet_z_hitz[:,0][(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]
        ratio = (jet_z[(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]).shape[0]/jet_z.shape[0]

        jet_z_plot = HistogramPlot(
            bins_range=(-200, 200),
            xlabel="$z$ Position",
            ylabel="Normalised number of jets",
            figsize=(6, 4.5),
            atlas_second_tag="$p_T=${}$\\pm ${}GeV".format(pt_0, pt_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100),
        )
        jet_z_plot.add(
            Histogram(
                jet_z_cut_true, label="True"
            )
        )
        jet_z_plot.add(
                Histogram(jet_z_cut_pred, label=reference)
        )
        jet_z_plot.draw()
        jet_z_plot.savefig("observables_" + reference + "/pt_{}_jet_z.png".format(pt_0))


############################################################################

# Looking at specific eta ranges


if Detailed_observable_plots:
    
    eta_vals=[-2,-1,0,1,2]
    eta_range=0.5

    for eta_0 in eta_vals:

        jet_z_cut_true = jet_z[(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]
        jet_z_cut_pred = jet_z_hitz[:,0][(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]
        ratio = (jet_z[(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]).shape[0]/jet_z.shape[0]

        jet_z_plot = HistogramPlot(
            bins_range=(-200, 200),
            xlabel="$z$ Position",
            ylabel="Normalised number of jets",
            figsize=(6, 4.5),
            atlas_second_tag="$\\eta=${}$\\pm ${}".format(eta_0, eta_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100),
        )
        jet_z_plot.add(
            Histogram(
                jet_z_cut_true, label="True"
            )
        )
        jet_z_plot.add(
                Histogram(jet_z_cut_pred, label=reference)
        )
        jet_z_plot.draw()
        jet_z_plot.savefig("observables_" + reference + "/eta_{}_jet_z.png".format(eta_0))


############################################################################

# Efficiency plots

if Common_efficiencies:
    sigmas = [1,3,5,10,20]

    jet_diff = jet_z-jet_z_hitz[:,0]
    jet_z_std = jet_z_hitz[:,1]
    bin_number = 40
    titles = [ "$(\\sigma<\\sigma_\\mathrm{{max}})$ / All Jets",
                "$(\\sigma<\\sigma_\\mathrm{{max}} \\wedge |z_t-z_p|<3\\sigma)$ / All Jets",
                "$(\\sigma<\\sigma_\\mathrm{{max}} \\wedge |z_t-z_p|<3\\sigma)$ / $(\\sigma<\\sigma_\\mathrm{{max}})$",]


    def calculate_efficiency(sigma_max, variable, bin_min, bin_max):
        
        N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
        N2, bin_edges = np.histogram(variable[jet_z_std<sigma_max], bins=bin_number, range=(bin_min,bin_max))
        N3, bin_edges = np.histogram(variable[(jet_z_std<sigma_max) & (np.abs(jet_diff)<3*jet_z_std)], bins=bin_number, range=(bin_min,bin_max))

        bins = (bin_edges[1:]+bin_edges[:bin_number])/2
        ratio1 = np.divide(N2, N1, out=np.zeros_like(N2, dtype=float), where=N1!=0, casting='unsafe')
        ratio2 = np.divide(N3, N1, out=np.zeros_like(N3, dtype=float), where=N1!=0, casting='unsafe')
        ratio3 = np.divide(N3, N2, out=np.zeros_like(N3, dtype=float), where=N2!=0, casting='unsafe')
        event_fraction = np.shape(variable[jet_z_std<sigma_max])[0]/np.shape(variable)[0]

        return ratio1, ratio2, ratio3, bins, event_fraction


    def calculate_efficiency_list(sigma_max, variable, bin_min, bin_max):
        
        r1 = []
        r2 = []
        r3 = []
        ev_frac = []

        for sig in sigma_max:
            ratio1, ratio2, ratio3, bins, event_fraction = calculate_efficiency(sig, variable, bin_min, bin_max)
            r1.append(ratio1)
            r2.append(ratio2)
            r3.append(ratio3)
            ev_frac.append(event_fraction)

        return r1, r2, r3, bins, ev_frac


    def efficiency_plot(sigma_max, variable, bin_min, bin_max, label):

        r1, r2, r3, bins, ev_frac = calculate_efficiency_list(sigma_max, variable, bin_min, bin_max)

        for j, ratio in enumerate([r1,r2,r3]):
            color = iter(plt.cm.rainbow(np.linspace(0, 1, len(sigma_max))))
            plt.figure(figsize=(12,8))
            for i, r in enumerate(ratio):
                c = next(color)
                plt.step(bins, r, color=c, label = '{:0.1f}$\%$ of jets with $\\sigma < \\sigma_\\mathrm{{max}} = ${}'.format(ev_frac[i]*100, sigma_max[i]), where='mid')
            plt.legend(fontsize=20, frameon=False)
            plt.title(titles[j], fontsize=20)
            plt.xlabel(label, fontsize=20)
            plt.ylabel("Efficiencies", fontsize=20)
            plt.ylim(0,1.1)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
            plt.savefig("observables_" + reference + "/" + label + "Efficiency_{}_.png".format(j), format='png', bbox_inches='tight')
            plt.close()

    efficiency_plot(sigmas, pt, 0, 200, "$p_T$")
    efficiency_plot(sigmas, eta, -3,3, "$\\eta$")
    efficiency_plot(sigmas, jet_z, -150,150, "$z$ Position")


############################################################################

# X sigma efficiencies

if Sigma_efficiencies:
    X_max = [1,2,3,5]

    jet_diff = jet_z-jet_z_hitz[:,0]
    jet_z_std = jet_z_hitz[:,1]
    bin_number = 40
    titles = [ "$(\\sigma<\\sigma_\\mathrm{{max}})$ / All Jets",
                "$(\\sigma<\\sigma_\\mathrm{{max}} \\wedge |z_t-z_p|<3\\sigma)$ / All Jets",
                "$(\\sigma<\\sigma_\\mathrm{{max}} \\wedge |z_t-z_p|<3\\sigma)$ / $(\\sigma<\\sigma_\\mathrm{{max}})$",]


    def calculate_efficiency_X(X, variable, bin_min, bin_max):
        
        N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
        N3, bin_edges = np.histogram(variable[np.abs(jet_diff)<X*jet_z_std], bins=bin_number, range=(bin_min,bin_max))

        bins = (bin_edges[1:]+bin_edges[:bin_number])/2
        ratio = np.divide(N3, N1, out=np.zeros_like(N3, dtype=float), where=N1!=0, casting='unsafe')
        event_fraction = np.shape(variable[np.abs(jet_diff)<X*jet_z_std])[0]/np.shape(variable)[0]

        return ratio, bins, event_fraction


    def calculate_efficiency_list_X(X_max, variable, bin_min, bin_max):
        
        r = []
        ev_frac = []

        for X in X_max:
            ratio, bins, event_fraction = calculate_efficiency_X(X, variable, bin_min, bin_max)
            r.append(ratio)
            ev_frac.append(event_fraction)

        return  r, bins, ev_frac


    def efficiency_plot_X(X_max, variable, bin_min, bin_max, label):

        r,  bins, ev_frac = calculate_efficiency_list_X(X_max, variable, bin_min, bin_max)

        color = iter(plt.cm.rainbow(np.linspace(0, 1, len(X_max))))
        plt.figure(figsize=(12,8))
        for i, ratio in enumerate(r):
            c = next(color)
            plt.step(bins, ratio, color=c, label = '{:0.1f}$\%$ of jets with $|z_t-z_p|<${}$\\sigma$'.format(ev_frac[i]*100, X_max[i]), where='mid')
        plt.legend(fontsize=20, frameon=False)
        plt.xlabel(label, fontsize=20)
        plt.ylabel("$|z_t-z_p|<X\\sigma$ / All Jets", fontsize=20)
        plt.ylim(0,1.1)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
        plt.savefig("observables_" + reference + "/" + label + "X_sigma_Efficiency.png", format='png', bbox_inches='tight')
        plt.close()

    efficiency_plot_X(X_max, pt, 0, 200, "$p_T$ [GeV]")
    efficiency_plot_X(X_max, eta, -3,3, "$\\eta$")
    efficiency_plot_X(X_max, jet_z, -150,150, "$z$ Position")


############################################################################

# Required sigma max

if Required_sigma_max_plots:

    efficiencies = [0.1,0.3,0.5,0.8, 0.9]

    jet_diff = jet_z-jet_z_hitz[:,0]
    jet_z_std = jet_z_hitz[:,1]
    bin_number = 40

    sigma_min = 0.
    sigma_step = 0.05

    def calculate_max_sigma(efficiencies, variable, bin_min, bin_max):
        
        N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
        bins = (bin_edges[1:]+bin_edges[:bin_number])/2
        sigma_borders = []
        for eff in efficiencies:
            N2 = np.zeros_like(N1, dtype=float)
            step = 0
            while step > -1:
                N, bin_edges = np.histogram(variable[jet_z_std<sigma_min+sigma_step*step], bins=bin_number, range=(bin_min,bin_max))
                for i in range(bin_number):
                    if N1[i]>0:
                        frac = N[i]/N1[i]
                        if frac > eff and N2[i]==0:
                            N2[i] += sigma_min+sigma_step*step
                if (np.min(N2[N1!=0]) != 0) or (sigma_min+sigma_step*step >11):
                    N2[N2==0] = 11
                    sigma_borders.append(N2)
                    break
                step += 1
        return sigma_borders, bins


    def efficiency_plot_max_sigma(efficiencies, variable, bin_min, bin_max, label):

        eff, bins = calculate_max_sigma(efficiencies, variable, bin_min, bin_max)

        color = iter(plt.cm.rainbow(np.linspace(0, 1, len(efficiencies))))
        plt.figure(figsize=(12,8))
        for i, efficiency in enumerate(eff):
            c = next(color)
            plt.step(bins, efficiency, color=c, label = 'Efficiency $\\geq$ {:0.0f}$\%$'.format( efficiencies[i]*100), where='mid')
        plt.legend(fontsize=20, frameon=False)
        plt.title("Required $\\sigma$ to get given Efficiency", fontsize=20)
        plt.xlabel(label, fontsize=20)
        plt.ylabel("$\\sigma_\\mathrm{{max}}$", fontsize=20)
        plt.ylim(0,10)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
        plt.savefig("observables_" + reference + "/" + label + "_max_sigma.png", format='png', bbox_inches='tight')
        plt.close()

    efficiency_plot_max_sigma(efficiencies, pt, 0, 200, "$p_T$")
    efficiency_plot_max_sigma(efficiencies, eta, -3,3, "$\\eta$")
    efficiency_plot_max_sigma(efficiencies, jet_z, -150,150, "$z$ Position")


############################################################################

# Required sigma deviation

if Required_sigma_plots:

    efficiencies = [0.1,0.3,0.5,0.8, 0.9]

    jet_diff = jet_z-jet_z_hitz[:,0]
    jet_z_std = jet_z_hitz[:,1]
    bin_number = 40

    sigma_min = 0.
    sigma_step = 0.05

    def calculate_sigma(efficiencies, variable, bin_min, bin_max):
        
        N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
        bins = (bin_edges[1:]+bin_edges[:bin_number])/2
        sigma_borders = []
        for eff in efficiencies:
            N2 = np.zeros_like(N1, dtype=float)
            step = 0
            while step > -1:
                N, bin_edges = np.histogram(variable[np.abs(jet_diff)<jet_z_std*(sigma_min+step*sigma_step)], bins=bin_number, range=(bin_min,bin_max))
                for i in range(bin_number):
                    if N1[i]>0:
                        frac = N[i]/N1[i]
                        if frac > eff and N2[i]==0:
                            N2[i] += sigma_min+sigma_step*step
                if (np.min(N2[N1!=0]) != 0) or (sigma_min+sigma_step*step >11):
                    N2[N2==0] = 11
                    sigma_borders.append(N2)
                    break
                step += 1
        return sigma_borders, bins


    def efficiency_plot_sigma(efficiencies, variable, bin_min, bin_max, label, ymax):

        eff, bins = calculate_sigma(efficiencies, variable, bin_min, bin_max)

        color = iter(plt.cm.rainbow(np.linspace(0, 1, len(efficiencies))))
        plt.figure(figsize=(12,8))
        for i, efficiency in enumerate(eff):
            c = next(color)
            plt.step(bins, efficiency, color=c, label = 'Efficiency $\\geq$ {:0.0f}$\%$'.format( efficiencies[i]*100), where='mid')
        plt.legend(fontsize=20, frameon=False)
        plt.title("Required $X$ value in $|z_t-z_p|<X\\sigma$ to get given Efficiency", fontsize=20)
        plt.xlabel(label, fontsize=20)
        plt.ylabel("$X$", fontsize=20)
        plt.ylim(0,ymax)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
        plt.savefig("observables_" + reference + "/" + label + "_X_sigma.png", format='png', bbox_inches='tight')
        plt.close()


    efficiency_plot_sigma(efficiencies, pt, 0, 200, "$p_T$", 2.5)
    efficiency_plot_sigma(efficiencies, eta, -3,3, "$\\eta$", 2.5)
    efficiency_plot_sigma(efficiencies, jet_z, -150,150, "$z$ Position", 5)


############################################################################

# Sigma over z plot

if sig_over_z:
    bin_min = -150
    bin_max = 150
    bin_number = 40
    width = (bin_max-bin_min)/bin_number
    jet_z_std = jet_z_hitz[:,1]
    jet_z_pred = jet_z_hitz[:,0]
    
    N1, bin_edges = np.histogram(jet_z, bins=bin_number, range=(bin_min,bin_max))
    N2 = np.zeros_like(N1, dtype=float)
    N3 = np.zeros_like(N1, dtype=float)
    bins = (bin_edges[1:]+bin_edges[:bin_number])/2

    for i in range(bin_number):
        if np.sum(jet_z_std[(jet_z > (bin_min + i*width)) & (jet_z < (bin_min + (i+1)*width))]) > 0:
            N2[i] += np.mean(jet_z_std[(jet_z > (bin_min + i*width)) & (jet_z < (bin_min + (i+1)*width))])
        if np.sum(jet_z_std[(jet_z_pred > (bin_min + i*width)) & (jet_z_pred < (bin_min + (i+1)*width))]) > 0:
            N3[i] += np.mean(jet_z_std[(jet_z_pred > (bin_min + i*width)) & (jet_z_pred < (bin_min + (i+1)*width))])


    plt.figure(figsize=(12,8))
    plt.step(bins, N2, color="blue", label = 'Average $\sigma$ over $z_t$', where='mid')
    plt.step(bins, N3, color="red", label = 'Average $\sigma$ over $z_p$', where='mid')
    plt.legend(fontsize=20, frameon=False)
    plt.title("Average $\sigma$ over truth and predicted $z$ value", fontsize=20)
    plt.xlabel("$z$", fontsize=20)
    plt.ylabel("Average $\sigma$", fontsize=20)
    plt.ylim(0,)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig("observables_" + reference + "/" + "av_sigma_over_z.png", format='png', bbox_inches='tight')
    plt.close()


############################################################################

# Normalised difference over observables plot

if diff_over_observable:
    
    def diff_over_obs(variable, bin_min, bin_max, label):
        
        bin_number = 40
        width = (bin_max-bin_min)/bin_number
        
        N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
        N2 = np.zeros_like(N1, dtype=float)
        bins = (bin_edges[1:]+bin_edges[:bin_number])/2

        for i in range(bin_number):
            jet_norm = np.abs(jet_z-jet_z_hitz[:,0])/jet_z_hitz[:,1]
            if np.sum(jet_norm[(variable > (bin_min + i*width)) & (variable < (bin_min + (i+1)*width))]) > 0:
                N2[i] += np.mean(jet_norm[(variable > (bin_min + i*width)) & (variable < (bin_min + (i+1)*width))])
        

        plt.figure(figsize=(12,8))
        plt.step(bins, N2, color="blue", label = r'$|z_t-z_p|/\sigma_p$', where='mid')
        plt.legend(fontsize=20, frameon=False)
        plt.title(r"$|z_t-z_p|/\sigma_p$ dependence", fontsize=20)
        plt.xlabel(label, fontsize=20)
        plt.ylabel("Normalised number of jets", fontsize=20)
        plt.ylim(0,)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.savefig("observables_" + reference + "/" + label + "norm_diff_over_variable.png", format='png', bbox_inches='tight')
        plt.close()

    diff_over_obs(pt, 0, 200, "$p_T$")
    diff_over_obs(eta, -3,3, "$\\eta$")
    diff_over_obs(jet_z, -150, 150, "$z$ position")


############################################################################

print("Plotting successful.")