########################################################################################################################################################

# Some useful plotting routines to evaluate network performance on a jet-by-jet basis

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

############################################################################

# DipZ or HitZ

# folder_name = "z_L1_jfex" 
# # network_name = "Dipz_BIG_ttbar_20240315-T180304"
# network_name = "Hitz_new_pp_large_lr_200_20240312-T090531"

# base_path = '/eos/user/b/backes/QT/preprocessing/'
# network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + network_name + "/ckpts/epoch=*." 

# test_path = glob.glob(base_path + folder_name + "/user.*.601229" + ".*.h5/*.h5")[0] 
# network_path = glob.glob(network_base + "37833660" + ".*.h5")[0]

# reference = "HitZ"
# # reference = "DipZ"


# folder_name = "Dipz_jfex" 
# network_name = "Hitz_new_pp_large_lr_200_20240312-T090531"

# base_path = '/eos/user/b/backes/QT/preprocessing/'
# network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + network_name + "/ckpts/epoch=*." 

# test_path = glob.glob(base_path + folder_name + "/user.*.601479" + ".*.h5/*.h5")[0] 
# network_path = glob.glob(network_base + "37826109" + ".*.h5")[0]

# reference = "HitZ"


test_path = '/eos/user/b/backes/QT/TDD/training-dataset-dumper/long_run.h5'
network_path = test_path

reference = "HitZ"


############################################################################

# Load test data

num_jets = -1

with h5py.File(test_path, 'r') as test_f:
    jets = test_f['jets'][:num_jets]
    jet_z = jets['TruthJetPVz']
    pt_MeV = jets['pt']
    pt = jets['pt']/1000
    eta = jets['eta']

with h5py.File(network_path, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred = jets["Hitz_gaussian_regression_TruthJetPVz"].reshape((-1,1))
    jet_z_stddev = jets["Hitz_gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))
    jet_z_hitz = np.append(jet_z_pred, jet_z_stddev, axis=1)

# cut away nans
jet_z_hitz = jet_z_hitz[~np.isnan(jet_z)]
pt = pt[~np.isnan(jet_z)]
eta = eta[~np.isnan(jet_z)]
jet_z = jet_z[~np.isnan(jet_z)]

############################################################################

# 2D Plot

plt.figure(figsize=(10,8))
h = plt.hist2d(jet_z, jet_z_hitz[:,0], bins=30, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("Truth $z_t$ [mm]", fontsize=20)
plt.ylabel("Predicted $z_p$ [mm]", fontsize=20)
plt.xlim(-149.9, 149.9)
plt.ylim(-149.9, 149.9)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [-150,150]
plt.plot(x,x,c='red')
hep.atlas.label(loc=4, data=True, label="Internal", rlabel='')
plt.savefig("observables_" + reference + "/2D_jet_z.png", format='png', bbox_inches='tight')
plt.close()


############################################################################


# Single Jet plot

def Gauss(x, mu, sigma):
    return 1/np.sqrt(2*np.pi*sigma**2) * np.exp(-(x-mu)**2/(2*sigma**2))

number_of_single_jets=10
color = iter(plt.cm.rainbow(np.linspace(0, 1, number_of_single_jets)))

plt.figure(figsize=(10,8))
x = np.linspace(-150,150, 100000)
print("Some single jets:")
for i in range(number_of_single_jets):
    i = i*10
    c = next(color)
    y = Gauss(x, jet_z_hitz[i,0], jet_z_hitz[i,1])
    plt.plot(x, y/np.max(y),
            label = "$z_t=${:0.1f}, $z_p=${:0.1f}, $\sigma_p=${:0.1f}".format(jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1],2),
            color=c)
    plt.vlines(jet_z[i], 0, 1, color=c, ls = '--')
    print("Jet {}:".format(i+1), jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1])

plt.xlabel("$z$ [mm]", fontsize=20)
plt.ylabel("Single jet distributions (not normalised)", fontsize=20)
plt.ylim(0,1.8)
plt.xlim(-119.9, 119.9)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
first_legend = plt.legend(loc="upper center", fontsize=15, ncol=2, frameon=False)
plt.gca().add_artist(first_legend)
line1, = plt.plot([], label="$z_t$", linestyle='--', linewidth=2, c='black')
line2, = plt.plot([], label="$G(z_p,\\sigma_p)$", linewidth=2, c='black')
plt.legend(handles=[line1, line2], loc=(0.35,0.6), fontsize=15, ncol=2, frameon=False)
hep.atlas.label(loc=0, data=True, label="Internal", rlabel='')
plt.savefig("observables_" + reference + "/Single_jets.png", format='png', bbox_inches='tight')
plt.close()


############################################################################
    
# pt, eta, z dependency plot

pt_vals=[40, 50, 90, 120, 150, 200]
pt_range=20
bin_min = -150
bin_max = 150
bin_number = 30

for pt_0 in pt_vals:

    jet_z_cut_true = jet_z[(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]
    jet_z_cut_pred = jet_z_hitz[:,0][(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]
    ratio = (jet_z[(pt > (pt_0-pt_range)) & (pt < (pt_0+pt_range))]).shape[0]/jet_z.shape[0]


    N1, bin_edges = np.histogram(jet_z_cut_true, bins=bin_number, range=(bin_min,bin_max), density=True)
    N2, bin_edges = np.histogram(jet_z_cut_pred, bins=bin_number, range=(bin_min,bin_max), density=True)
    bins = (bin_edges[1:]+bin_edges[:bin_number])/2
    
    plt.figure(figsize=(10,8))
    plt.step(bins, N1, color="red", label = 'Truth', where='mid')
    plt.step(bins, N2, color="blue", label = "HitZ", where='mid')
    plt.legend(fontsize=20, frameon=False)
    plt.xlabel("$z$ [mm]", fontsize=20)
    plt.ylabel("Normalised number of jets", fontsize=20)
    plt.ylim(0,)
    plt.xlim(-149.9, 149.9)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    hep.atlas.label(loc=4, data=True, label="Internal", rlabel ="$p_T=${}$\\pm ${}GeV".format(pt_0, pt_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100), fontsize=20)
    plt.savefig("observables_" + reference + "/pt_{}_jet_z.png".format(pt_0), format='png', bbox_inches='tight')
    plt.close()
    
eta_vals=[-2,-1,0,1,2]
eta_range=0.5

for eta_0 in eta_vals:

    jet_z_cut_true = jet_z[(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]
    jet_z_cut_pred = jet_z_hitz[:,0][(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]
    ratio = (jet_z[(eta > (eta_0-eta_range)) & (eta < (eta_0+eta_range))]).shape[0]/jet_z.shape[0]

    N1, bin_edges = np.histogram(jet_z_cut_true, bins=bin_number, range=(bin_min,bin_max), density=True)
    N2, bin_edges = np.histogram(jet_z_cut_pred, bins=bin_number, range=(bin_min,bin_max), density=True)
    bins = (bin_edges[1:]+bin_edges[:bin_number])/2
    
    plt.figure(figsize=(10,8))
    plt.step(bins, N1, color="red", label = 'Truth', where='mid')
    plt.step(bins, N2, color="blue", label = "HitZ", where='mid')
    plt.legend(fontsize=20, frameon=False)
    plt.xlabel("$z$ [mm]", fontsize=20)
    plt.ylabel("Normalised number of jets", fontsize=20)
    plt.ylim(0,)
    plt.xlim(-149.9, 149.9)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    hep.atlas.label(loc=4, data=True, label="Internal", rlabel ="$\\eta=${}$\\pm ${}".format(eta_0, eta_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100))
    plt.savefig("observables_" + reference + "/eta_{}_jet_z.png".format(eta_0), format='png', bbox_inches='tight')
    plt.close()

z_vals=[]#[-100,-50,0,50, 100]
z_range=20

for z in z_vals:
    jet_z_cut_true = jet_z[(jet_z > (z-z_range)) & (jet_z < (z+z_range))]
    jet_z_cut_pred = jet_z_hitz[:,0][(jet_z > (z-z_range)) & (jet_z < (z+z_range))]
    ratio = (jet_z[(jet_z > (z-z_range)) & (jet_z < (z+z_range))]).shape[0]/jet_z.shape[0]

    N1, bin_edges = np.histogram(jet_z_cut_true, bins=bin_number, range=(bin_min,bin_max), density=True)
    N2, bin_edges = np.histogram(jet_z_cut_pred, bins=bin_number, range=(bin_min,bin_max), density=True)
    bins = (bin_edges[1:]+bin_edges[:bin_number])/2
    
    plt.figure(figsize=(10,8))
    plt.step(bins, N1, color="red", label = 'Truth', where='mid')
    plt.step(bins, N2, color="blue", label = "HitZ", where='mid')
    plt.legend(fontsize=20, frameon=False)
    plt.xlabel("$z$ [mm]", fontsize=20)
    plt.ylabel("Normalised number of jets", fontsize=20)
    plt.ylim(0,)
    plt.xlim(-149.9, 149.9)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    hep.atlas.label(loc=4, data=True, label="Internal", rlabel ="$z_t=${}$\\pm ${}".format(z, z_range) + "\n" + "$\\approx${:0.1f}$\%$ of total jets".format(ratio*100))
    # plt.savefig("observables_" + reference + "/{}_jet_z.png".format(z), format='png', bbox_inches='tight')
    plt.close()


############################################################################

# X sigma efficiencies

X_max = [1,2,3,5]

jet_diff = jet_z-jet_z_hitz[:,0]
jet_z_std = jet_z_hitz[:,1]
bin_number = 40
titles = [ "$(\\sigma<\\sigma_\\mathrm{{max}})$ / All Jets",
            "$(\\sigma<\\sigma_\\mathrm{{max}} \\wedge |z_t-z_p|<3\\sigma)$ / All Jets",
            "$(\\sigma<\\sigma_\\mathrm{{max}} \\wedge |z_t-z_p|<3\\sigma)$ / $(\\sigma<\\sigma_\\mathrm{{max}})$",]


def calculate_efficiency_X(X, variable, bin_min, bin_max):
    
    N1, bin_edges = np.histogram(variable, bins=bin_number, range=(bin_min,bin_max))
    N3, bin_edges = np.histogram(variable[np.abs(jet_diff)<X*jet_z_std], bins=bin_number, range=(bin_min,bin_max))

    bins = (bin_edges[1:]+bin_edges[:bin_number])/2
    ratio = np.divide(N3, N1, out=np.zeros_like(N3, dtype=float), where=N1!=0, casting='unsafe')
    event_fraction = np.shape(variable[np.abs(jet_diff)<X*jet_z_std])[0]/np.shape(variable)[0]

    return ratio, bins, event_fraction


def calculate_efficiency_list_X(X_max, variable, bin_min, bin_max):
    
    r = []
    ev_frac = []

    for X in X_max:
        ratio, bins, event_fraction = calculate_efficiency_X(X, variable, bin_min, bin_max)
        r.append(ratio)
        ev_frac.append(event_fraction)

    return  r, bins, ev_frac


def efficiency_plot_X(X_max, variable, bin_min, bin_max, label):

    r,  bins, ev_frac = calculate_efficiency_list_X(X_max, variable, bin_min, bin_max)

    color = iter(plt.cm.rainbow(np.linspace(0, 1, len(X_max))))
    plt.figure(figsize=(12,8))
    for i, ratio in enumerate(r):
        c = next(color)
        plt.step(bins, ratio, color=c, label = '{:0.1f}$\%$ of jets with $|z_t-z_p|<${}$\\sigma_p$'.format(ev_frac[i]*100, X_max[i]), where='mid')
    plt.legend(fontsize=20, frameon=False)
    plt.xlabel(label, fontsize=20)
    plt.ylabel("$|z_t-z_p|<X\\sigma_p$ / All Jets", fontsize=20)
    plt.ylim(0,1.1)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    hep.atlas.label(loc=0, data=True, label="Internal", rlabel='')
    plt.savefig("observables_" + reference + "/" + label + "X_sigma_Efficiency.png", format='png', bbox_inches='tight')
    plt.close()

efficiency_plot_X(X_max, pt, 0, 200, "$p_T$ [GeV]")
efficiency_plot_X(X_max, eta, -3,3, "$\\eta$")
# efficiency_plot_X(X_max, jet_z, -150,150, "$z$ Position")
# efficiency_plot_X(X_max, jet_z_hitz[:,1], 0, 40, "$\sigma_p$")


############################################################################

# Sigma over pt plot

bin_min = 0
bin_max = 300
bin_number = 60
width = (bin_max-bin_min)/bin_number
jet_z_std = jet_z_hitz[:,1]
jet_z_pred = jet_z_hitz[:,0]

N1, bin_edges = np.histogram(pt, bins=bin_number, range=(bin_min,bin_max))
N2 = np.zeros_like(N1, dtype=float)
bins = (bin_edges[1:]+bin_edges[:bin_number])/2

for i in range(bin_number):
    if np.sum(jet_z_std[(pt > (bin_min + i*width)) & (pt < (bin_min + (i+1)*width))]) > 0:
        N2[i] += np.mean(jet_z_std[(pt > (bin_min + i*width)) & (pt < (bin_min + (i+1)*width))])
    

plt.figure(figsize=(12,8))
plt.step(bins, N2, color="blue", label = 'Average $\sigma$ over $p_t$', where='mid')
plt.legend(fontsize=20, frameon=False)
plt.title("Average $\sigma$", fontsize=20)
plt.xlabel("$p_T$", fontsize=20)
plt.ylabel("Average $\sigma$", fontsize=20)
plt.ylim(0,)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=0, data=True, label="Internal", rlabel='')
# plt.savefig("observables_" + reference + "/" + "av_sigma_over_z.png", format='png', bbox_inches='tight')
plt.close()


############################################################################

# Pull plot

g1 = np.random.normal(0,1,100000)
N1, bin_edges = np.histogram((jet_z-jet_z_pred)/jet_z_stddev, bins=20, range=(-5,5), density=True)
N2, bin_edges = np.histogram(g1, bins=20, range=(-5,5), density=True)
bins = (bin_edges[1:]+bin_edges[:20])/2

plt.figure(figsize=(10,8))
plt.step(bins, N2, color="blue", label = r'$G(\mu=0,\sigma=1)$', where='mid')
plt.step(bins, N1, color="red", label = r"HitZ $(z_t-z_p)/\sigma_p$", where='mid')
plt.legend(fontsize=20, frameon=False)
plt.xlabel(r"$(z_t-z_p)/\sigma_p$", fontsize=20)
plt.ylabel("Normalised number of jets", fontsize=20)
plt.ylim(0,)
plt.xlim(-5, 5)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Internal", rlabel='', fontsize=20)
plt.savefig("observables_" + reference + "/pull_plot.png", format='png', bbox_inches='tight')
plt.close()


############################################################################

print("Plotting successful.")


