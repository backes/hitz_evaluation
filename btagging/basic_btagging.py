########################################################################################################################################################

# Hit-information based b-tagging

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

print("\n=========================== \n" + " HitZ B-Tagging \n" + "=========================== \n")

########################################################################################################################################################

# Loading data

base_path = '/eos/user/b/backes/QT/preprocessing/'

# L1 trigger data
# network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "HitZ_btagging_with_z_128_weight_20240325-T180623" + "/ckpts/epoch=*." 
# test_path_1 = glob.glob(base_path + "L1_btagging/" + "pp_output_test_ttbar.h5")[0]
# network_path_1 = glob.glob(network_base +"*_test_ttbar.h5")[0]

# Jet Constituents tests
network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "BTag_hit_20240429-T153631" + "/ckpts/epoch=*." 
# network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "BTag_hit_jC_20240429-T145111" + "/ckpts/epoch=*." 
# network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "BTag_jC_without_z_20240429-T144545" + "/ckpts/epoch=*." 
test_path_1 = glob.glob(base_path + "jet_const/" + "pp_output_test_ttbar.h5")[0]
network_path_1 = glob.glob(network_base +"*_test_ttbar.h5")[0]


num_jets = -1 


with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    tag_truth = jets_sig['HadronConeExclTruthLabelID']
with h5py.File(network_path_1, 'r') as f:
    jets = f['jets'][:num_jets]
    pb = jets["BTag_hit_pb"]
    pc = jets["BTag_hit_pc"]
    pu = jets["BTag_hit_pu"]


########################################################################################################################################################

# Split test data and calculate discriminant Db

def calculate_Db(pb,pc,pu,fc=0.07):
    Db = np.log(pb/(fc*pc+(1-fc)*pu))
    return Db

pb_sig = pb[tag_truth==5]
pc_sig = pc[tag_truth==5]
pu_sig = pu[tag_truth==5]

pb_c = pb[tag_truth==4]
pc_c = pc[tag_truth==4]
pu_c = pu[tag_truth==4]

pb_u = pb[tag_truth==0]
pc_u = pc[tag_truth==0]
pu_u = pu[tag_truth==0]

Db_sig = calculate_Db(pb_sig,pc_sig,pu_sig)
Db_c = calculate_Db(pb_c,pc_c,pu_c)
Db_u = calculate_Db(pb_u,pc_u,pu_u)


########################################################################################################################################################

# Plot Db

bin_min = -5
bin_max = 5

db_sig_plot, bins = np.histogram(Db_sig, bins=100, range=(bin_min, bin_max), density=True)
db_c_plot, bins = np.histogram(Db_c, bins=100, range=(bin_min, bin_max), density=True)
db_u_plot, bins = np.histogram(Db_u, bins=100, range=(bin_min, bin_max), density=True)
         

plt.figure(figsize=(12,8))
plt.step(bins[1:], db_sig_plot, c='red', label="$D_b$(bjets))")
plt.step(bins[1:], db_c_plot, c='orange', label="$D_b$(cjets)")
plt.step(bins[1:], db_u_plot, c='blue', label="$D_b$(ujets)")
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.ylim(0,1)
plt.xlabel("$D_b$", fontsize=20)
plt.ylabel("Normalized Number of Jets", fontsize=20)
plt.legend(fontsize=20, frameon=False)
hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
plt.savefig("Plots/Hit_Db.png", format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

# Calculate and plot ROC curves

x = np.linspace(bin_min, bin_max, 10000, dtype = float)
b_eff = np.array([])
c_eff = np.array([])
u_eff = np.array([])

for x_val in x:
    b_eff = np.append(b_eff, Db_sig[Db_sig>x_val].shape[0]/Db_sig.shape[0])
    c_eff = np.append(c_eff, Db_c[Db_c>x_val].shape[0]/Db_c.shape[0])
    u_eff = np.append(u_eff, Db_u[Db_u>x_val].shape[0]/Db_u.shape[0])
      
x_ref = np.linspace(0.01, 1, 100000)

plt.figure(figsize=(12,8))
plt.rc('axes', axisbelow=True)
plt.grid(True, which="major", axis="x")
plt.grid(True, which="both", axis="y")

plt.plot(b_eff, 1/c_eff, color='blue', label="c-jet rejection")
plt.plot(b_eff, 1/u_eff, color='green', label="light jet rejection")
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.xlabel(r"b-Tag Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(0.6, 1)
plt.ylim(0.9,2e1)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
plt.savefig("Plots/Hit_btag_Roc_curve.png" , format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################
print("Evaluation successful.")