########################################################################################################################################################

# Plot dependency of HitZ pile-up rejection on the leading pT of the event

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import itertools
from matplotlib import colors
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

print("\n=========================== \n" + " pT dependencies \n" + "=========================== \n")

########################################################################################################################################################

# Loading data

base_path = '/eos/user/b/backes/QT/preprocessing/'
network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "Hitz_new_pp_large_lr_200_20240312-T090531" + "/ckpts/epoch=*." 
folder_name = "z_L1_jfex"

# test_path_1 = glob.glob(base_path + "Dipz_jfex/user" + ".*.h5/*.h5")[0]
test_path_1 = glob.glob(base_path + folder_name + "/user.*.601479" + ".*.h5/*.h5")[0]
test_path_2 = glob.glob(base_path + folder_name + "/user.*.801167" + ".*.h5/*.h5")[0]
test_path_3 = glob.glob(base_path + folder_name + "/user.*.801168" + ".*.h5/*.h5")[0]

network_path_1 = glob.glob(network_base + "37826109" + ".*.h5")[0]
network_path_2 = glob.glob(network_base + "37833659" + ".*.h5")[0]
network_path_3 = glob.glob(network_base + "37833661" + ".*.h5")[0]

num_jets = -1 #10000

n = 4
m = 6

# Signal

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    TruthZ_sig = jets_sig['TruthJetPVz']
    HadronConeExclTruthLabelID_sig = jets_sig['HadronConeExclTruthLabelID']
    
with h5py.File(network_path_1, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"]


# Background

with h5py.File(test_path_2, 'r') as test_f:
    jets_jz2 = test_f['jets'][:num_jets]
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eventNumber_jz2 = jets_jz2['eventNumber']
    mc_weight_jz2 = jets_jz2['mcEventWeight']
    TruthZ_jz2 = jets_jz2['TruthJetPVz']
with h5py.File(network_path_2, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_jz2 = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_jz2 = jets["gaussian_regression_TruthJetPVz_stddev"]

with h5py.File(test_path_3, 'r') as test_f:
    jets_jz3 = test_f['jets'][:num_jets]
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eventNumber_jz3 = jets_jz3['eventNumber']
    mc_weight_jz3 = jets_jz3['mcEventWeight']
    TruthZ_jz3 = jets_jz3['TruthJetPVz']
with h5py.File(network_path_3, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_jz3 = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_jz3 = jets["gaussian_regression_TruthJetPVz_stddev"]

TruthZ_bkg = np.concatenate((TruthZ_jz2, TruthZ_jz3))
eventNumber_bkg = np.concatenate((eventNumber_jz2, eventNumber_jz3))
pt_bkg = np.concatenate((pt_jz2,pt_jz3))
jet_z_pred_bkg = np.concatenate((jet_z_pred_jz2,jet_z_pred_jz3))
jet_z_stddev_bkg = np.concatenate((jet_z_stddev_jz2,jet_z_stddev_jz3))


# Calculate MC weights

cs_jz2 = 2582600000.0 *1e-12
filter_jz2 = 1.006522E-02 
cs_jz3 = 28528000.0 *1e-12  
filter_jz3 = 1.190844E-02 
_, unique_events_jz2 = np.unique(eventNumber_jz2, return_index=True)
total_events_jz2 = np.sum(mc_weight_jz2[unique_events_jz2])
_, unique_events_jz3 = np.unique(eventNumber_jz3, return_index=True)
total_events_jz3 = np.sum(mc_weight_jz3[unique_events_jz3])
weight_jz2 = cs_jz2 * filter_jz2 / total_events_jz2 *50000
weight_jz3 = cs_jz3 * filter_jz3 / total_events_jz3 *50000

mc_weight_bkg = np.concatenate((mc_weight_jz2 * weight_jz2, mc_weight_jz3 * weight_jz3))


########################################################################################################################################################

# Include 4 bjet condition for signal

count = 0
sum_counter = 0
for id in np.unique(eventNumber_sig):
    jets = HadronConeExclTruthLabelID_sig[eventNumber_sig == id]
    bjets = jets[jets == 5]
    if len(eventNumber_sig[eventNumber_sig == id]) < 4:
        sum_counter += 1
    if len(bjets) < 4 :
        mc_weight_sig[eventNumber_sig == id] = 0.
        count += 1

print("4-jet-condition done. Threw away {:0.2f}% of the test data".format(100*sum_counter/len(np.unique(eventNumber_sig))))
print("4-b-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*count/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Implementation of MLPL calculation

def calculate_MLPL(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std = std[:m]

    L_max = -9999999999.
    z_MLPL = 0.
    sig_MLPL = 0.

    for subset in itertools.combinations(range(m), n):
        log_l = -0.5*np.log(2*np.pi)*n
        log_l += -np.sum(np.log(std[[subset]]), dtype=float)
        term1 = np.sum(np.divide(pred[[subset]], std[[subset]]**2, dtype=float), dtype=float)
        term2 = np.sum(np.divide(np.ones_like(std[[subset]],dtype=float), std[[subset]]**2, dtype=float), dtype=float)
        log_l += -np.sum( np.divide((term1/term2 - pred[[subset]])**2,(2.*std[[subset]]**2), dtype=float))
        if log_l > L_max:
            L_max = log_l
            z_MLPL = term1/term2
            sig_MLPL = np.sqrt(1./term2)

    return z_MLPL, sig_MLPL, L_max, cur_pt[cur_eN==ev_num][this_event_pt[::-1]][0]


def evaluate_MLPL(eventNumber_atm, pt_atm, jet_z_pred_atm, jet_z_stddev_atm, TruthZ_atm, mc_weight_atm):
    
    z_MLPL_arr = np.array([])
    sig_MLPL_arr = np.array([])
    truth_z_arr = np.array([])
    mc_weight_arr = np.array([])
    leading_pt_arr = np.array([])
    L_val = np.array([])

    for ev_num in np.unique(eventNumber_atm):
        jets = eventNumber_atm[eventNumber_atm==ev_num].shape[0]
        if jets >= n:
            if m == "All":
                z_MLPL, sig_MLPL, L_max, pt_leading = calculate_MLPL(ev_num, n, jets, eventNumber_atm, pt_atm, jet_z_pred_atm, jet_z_stddev_atm)
            else:
                if jets >= m: 
                    z_MLPL, sig_MLPL, L_max, pt_leading = calculate_MLPL(ev_num, n, m, eventNumber_atm, pt_atm, jet_z_pred_atm, jet_z_stddev_atm)
                else:
                    z_MLPL, sig_MLPL, L_max, pt_leading = calculate_MLPL(ev_num, n, jets, eventNumber_atm, pt_atm, jet_z_pred_atm, jet_z_stddev_atm)

            z_MLPL_arr = np.append(z_MLPL_arr, z_MLPL)
            sig_MLPL_arr = np.append(sig_MLPL_arr, sig_MLPL)
            mc_weight_arr = np.append(mc_weight_arr, mc_weight_atm[eventNumber_atm==ev_num][0])
            leading_pt_arr = np.append(leading_pt_arr, pt_leading)
            L_val = np.append(L_val, L_max)
            
            values, counts = np.unique(TruthZ_atm[eventNumber_atm == ev_num], return_counts=True)
            truth_z_value = values[np.argmax(counts)]
            truth_z_arr = np.append(truth_z_arr, truth_z_value)

    return z_MLPL_arr, sig_MLPL_arr, truth_z_arr, mc_weight_arr, leading_pt_arr, L_val


z_MLPL_arr, sig_MLPL_arr, truth_z_arr, mc_weight_arr, leading_pt_arr, L_val = evaluate_MLPL(eventNumber_sig, 
                                                                                            pt_sig, jet_z_pred_sig, jet_z_stddev_sig, 
                                                                                            TruthZ_sig, mc_weight_sig)
# z_MLPL_arr_bkg, sig_MLPL_arr_bkg, truth_z_arr_bkg, mc_weight_arr_bkg, leading_pt_arr_bkg, L_val_bkg = evaluate_MLPL(eventNumber_bkg, 
                                                                                                                    # pt_bkg, jet_z_pred_bkg, 
                                                                                                                    # jet_z_stddev_bkg, TruthZ_bkg, mc_weight_bkg)

_,_,_, mc_weight_arr_bkg,_, L_val_bkg = evaluate_MLPL(eventNumber_bkg, pt_bkg, jet_z_pred_bkg, jet_z_stddev_bkg, TruthZ_bkg, mc_weight_bkg)


########################################################################################################################################################

# Plotting for different Working Points

#WP = -17.1 #-17.8

for WP in [-15, -16, -17, -17.1, -18]:

    bkg_rejection = np.sum(mc_weight_arr_bkg)/np.sum(mc_weight_arr_bkg[L_val_bkg>WP])

    bin_min = 0
    bin_max = 300
    bin_number = 30

    pt_full, bins = np.histogram(leading_pt_arr, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_arr)
    pt_cut, bins = np.histogram(leading_pt_arr[L_val > WP], bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_arr[L_val > WP])
    pt_full[pt_full==0] = np.nan
    ratio = pt_cut/pt_full

    fig, axs = plt.subplots(2,1,figsize=(12,8), gridspec_kw={'height_ratios': [4, 1], 'hspace':0}, sharex=False, tight_layout=True)
    hep.atlas.label(ax = axs[0], loc=4, data=True, label="Work in Progress", rlabel='')

    axs[0].step([],[],c="white", label="WP = {}".format(WP))
    axs[0].step([],[],c="white", label=r"$\varepsilon_{sig}=$"+"{:0.2f}%".format(100*pt_cut.sum()/pt_full.sum()))
    axs[0].step([],[],c="white", label=r"$1/\varepsilon_{bkg}=$"+"{:0.2f}".format(bkg_rejection))
    axs[0].step(bins[1:], pt_full, c='red', label="Full $p_T$")
    axs[0].step(bins[1:], pt_cut, c='blue', label="$p_T$ with $L_{max}>$WP")

    axs[0].set_ylabel("Number of Events", fontsize=20)
    axs[0].legend(frameon=False, fontsize= 20)
    axs[0].set_xlim(bin_min,bin_max)
    axs[0].set_ylim(0,)
    axs[0].set_xticks([],[],fontsize=20)
    axs[0].yaxis.set_tick_params(labelsize=20)

    axs[1].step((bins[1:]+bins[:bin_number])/2, ratio, c='black', where='mid')
    axs[1].set_ylim([0.95,1.02])
    axs[1].yaxis.set_tick_params(labelsize=20)
    axs[1].set_xticks(np.linspace(bin_min,bin_max,7), np.linspace(bin_min,bin_max,7, dtype=int), fontsize=20)
    axs[1].set_xlabel("Leading $p_T$", fontsize=20)
    axs[1].set_ylabel("Ratio", fontsize=20)
    axs[1].grid(True, which='both', axis='y')
    axs[1].set_xlim(bin_min,bin_max)

    plt.savefig("WP_pt_dependency/WP_{}_pt_dependency.png".format(WP), format='png', bbox_inches='tight')


########################################################################################################################################################

print("Evaluation successful.")