########################################################################################################################################################

# Compare the MLPL prediction to the truth-level PVz on an event-by-event basis

########################################################################################################################################################

# Required packages

import h5py
import numpy as np
import matplotlib.pyplot as plt
import itertools
from matplotlib import colors
import glob
import mplhep as hep
hep.style.use(hep.style.ATLAS)

print("\n=========================== \n" + " Calculation of ROC curves \n" + "=========================== \n")

########################################################################################################################################################

# Loading data

base_path = '/eos/user/b/backes/QT/preprocessing/'
network_base = '/eos/user/b/backes/QT/salt/salt/logs/' + "Hitz_new_pp_large_lr_200_20240312-T090531" + "/ckpts/epoch=*." 

# test_path_1 = glob.glob(base_path + "Dipz_jfex/user" + ".*.h5/*.h5")[0] # jfexed DiHiggs
# network_path_1 = glob.glob(network_base + "37826109" + ".*.h5")[0]

test_path_1 = glob.glob(base_path + "new_preprocessing/pp_output_test_ttbar.h5")[0] # basic ttbar
network_path_1 = glob.glob(network_base + "*test_ttbar.h5")[0]


num_jets = -1

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    TruthZ_sig = jets_sig['TruthJetPVz']
    # HadronConeExclTruthLabelID_sig = jets_sig['HadronConeExclTruthLabelID']
    
with h5py.File(network_path_1, 'r') as f:
    jets = f['jets'][:num_jets]
    jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"]
    jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"]



########################################################################################################################################################

# Include 4 jet condition for signal

# count_throw_away = 0
# sum_counter = 0 
# for id in np.unique(eventNumber_sig):
#     _, counts = np.unique(TruthZ_sig[eventNumber_sig == id], return_counts=True)
#     if len(eventNumber_sig[eventNumber_sig == id]) < 4:
#         sum_counter += 1
#     if max(counts) < 4 :
#         mc_weight_sig[eventNumber_sig == id] = 0.
#         count_throw_away += 1

# print("4-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*sum_counter/len(np.unique(eventNumber_sig))))
# print("4-jet-condition from one vertex done. Threw away {:0.2f}% of the test data.".format(100*count_throw_away/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Include 4 bjet condition for signal

# count = 0
# sum_counter = 0
# for id in np.unique(eventNumber_sig):
#     jets = HadronConeExclTruthLabelID_sig[eventNumber_sig == id]
#     bjets = jets[jets == 5]
#     if len(eventNumber_sig[eventNumber_sig == id]) < 4:
#         sum_counter += 1
#     if len(bjets) < 4 :
#         mc_weight_sig[eventNumber_sig == id] = 0.
#         count += 1

# print("4-jet-condition done. Threw away {:0.2f}% of the test data".format(100*sum_counter/len(np.unique(eventNumber_sig))))
# print("4-b-jet-condition done. Threw away {:0.2f}% of the test data.".format(100*count/len(np.unique(eventNumber_sig))))


########################################################################################################################################################

# Implementation of MLPL calculation

def calculate_MLPL(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std = std[:m]

    L_max = -9999999999.
    z_MLPL = 0.
    sig_MLPL = 0.

    for subset in itertools.combinations(range(m), n):
        log_l = -0.5*np.log(2*np.pi)*n
        log_l += -np.sum(np.log(std[[subset]]), dtype=float)
        term1 = np.sum(np.divide(pred[[subset]], std[[subset]]**2, dtype=float), dtype=float)
        term2 = np.sum(np.divide(np.ones_like(std[[subset]],dtype=float), std[[subset]]**2, dtype=float), dtype=float)
        log_l += -np.sum( np.divide((term1/term2 - pred[[subset]])**2,(2.*std[[subset]]**2), dtype=float))
        if log_l > L_max:
            L_max = log_l
            z_MLPL = term1/term2
            sig_MLPL = np.sqrt(1./term2)

    return z_MLPL, sig_MLPL


n = 4
m = 6

z_MLPL_arr = np.array([])
sig_MLPL_arr = np.array([])
truth_z_arr = np.array([])
mc_weight_arr = np.array([])


for ev_num in np.unique(eventNumber_sig):
    jets = eventNumber_sig[eventNumber_sig==ev_num].shape[0]
    if jets >= n:

        if m == "All":
            z_MLPL, sig_MLPL = calculate_MLPL(ev_num, n, jets, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)
        else:
            if jets >= m: 
                z_MLPL, sig_MLPL = calculate_MLPL(ev_num, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)
            else:
                z_MLPL, sig_MLPL = calculate_MLPL(ev_num, n, jets, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig)

        z_MLPL_arr = np.append(z_MLPL_arr, z_MLPL)
        sig_MLPL_arr = np.append(sig_MLPL_arr, sig_MLPL)
        mc_weight_arr = np.append(mc_weight_arr, mc_weight_sig[eventNumber_sig==ev_num][0])

        values, counts = np.unique(TruthZ_sig[eventNumber_sig == ev_num], return_counts=True)
        truth_z_value = values[np.argmax(counts)]

        truth_z_arr = np.append(truth_z_arr, truth_z_value)


########################################################################################################################################################

# Getting the data

z_MLPL_arr = z_MLPL_arr[~np.isnan(truth_z_arr)]
sig_MLPL_arr = sig_MLPL_arr[~np.isnan(truth_z_arr)]
truth_z_arr = truth_z_arr[~np.isnan(truth_z_arr)]

print(z_MLPL_arr[np.abs(truth_z_arr-z_MLPL_arr)>15*sig_MLPL_arr])
print(sig_MLPL_arr[np.abs(truth_z_arr-z_MLPL_arr)>15*sig_MLPL_arr])
print(truth_z_arr[np.abs(truth_z_arr-z_MLPL_arr)>15*sig_MLPL_arr])


########################################################################################################################################################

# Sigma efficiency

X_arr = np.linspace(0,15,151)

X_sigma_mean = X_arr * np.mean(sig_MLPL_arr)
event_fraction = np.array([])
for X in X_arr:
    event_fraction = np.append(event_fraction, np.shape(sig_MLPL_arr[np.abs(truth_z_arr-z_MLPL_arr)<X*sig_MLPL_arr])[0]/np.shape(sig_MLPL_arr)[0])


plt.figure(figsize=(12,8))
plt.rc('axes', axisbelow=True)
plt.grid(True, which="major", axis="x")
plt.grid(True, which="both", axis="y")
plt.plot(X_sigma_mean, event_fraction, marker='.', color="blue", zorder=1)

plt.scatter([], [], marker='.',linewidth=0, color="white", label=r"$\overline{\sigma_{MLPL}}=$"+"{:0.2f}".format(np.mean(sig_MLPL_arr)), zorder=2)
plt.scatter(X_sigma_mean[X_arr==1.], event_fraction[X_arr==1.], marker='o',linewidth=4, color="orange", label="$X=1$", zorder=2)
plt.scatter(X_sigma_mean[X_arr==2.], event_fraction[X_arr==2.], marker='o',linewidth=4, color="green", label="$X=2$", zorder=2)
plt.scatter(X_sigma_mean[X_arr==3.], event_fraction[X_arr==3.], marker='o',linewidth=4, color="red", label="$X=3$", zorder=2)
plt.scatter(X_sigma_mean[X_arr==5.], event_fraction[X_arr==5.], marker='o',linewidth=4, color="purple", label="$X=5$", zorder=2)
plt.scatter(X_sigma_mean[X_arr==10.], event_fraction[X_arr==10.], marker='o',linewidth=4, color="black", label="$X=10$", zorder=2)

plt.xlabel(r"$X\cdot \overline{\sigma_{MLPL}}$", fontsize=20)
plt.ylabel(r"Fraction of Events with $|z_t-z_{MLPL}|<X\cdot \sigma_MLPL$", fontsize=20)
plt.ylim(0,1)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.legend(fontsize=20, frameon=True, loc='lower right')
hep.atlas.label(loc=0, data=True, label="Work in Progress", rlabel='')
plt.savefig("PVz/efficiency.png" , format='png', bbox_inches='tight')
plt.close()

########################################################################################################################################################

# 2D Plot

plt.figure(figsize=(10,8))
h = plt.hist2d(truth_z_arr, z_MLPL_arr, bins=30, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("Truth $z_t$ [mm]", fontsize=20)
plt.ylabel("Predicted $z_p$ [mm]", fontsize=20)
plt.xlim(-149.9, 149.9)
plt.ylim(-149.9, 149.9)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [-150,150]
plt.plot(x,x,c='red')
hep.atlas.label(loc=4, data=True, label="Work in Progress", rlabel='')
plt.savefig("PVz/2D_jet_z.png", format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

# z Plot

bin_min = -150
bin_max = 150
bin_number = 30

N1, bin_edges = np.histogram(truth_z_arr, bins=bin_number, range=(bin_min,bin_max), density=True)
N2, bin_edges = np.histogram(z_MLPL_arr, bins=bin_number, range=(bin_min,bin_max), density=True)
bins = (bin_edges[1:]+bin_edges[:bin_number])/2

plt.figure(figsize=(10,8))
plt.step(bins, N1, color="red", label = '$z_T$', where='mid')
plt.step(bins, N2, color="blue", label = "$z_{MLPL}$", where='mid')
plt.legend(fontsize=20, frameon=False)
plt.xlabel("$z$ [mm]", fontsize=20)
plt.ylabel("Normalised number of Events", fontsize=20)
plt.ylim(0,)
plt.xlim(-149.9, 149.9)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress",rlabel='', fontsize=20)
plt.savefig("PVz/z_dist.png", format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

# z Difference

bin_min = -5
bin_max = 5
bin_number = 30

N3, bin_edges = np.histogram(truth_z_arr-z_MLPL_arr, bins=bin_number, range=(bin_min,bin_max), density=True)
bins = (bin_edges[1:]+bin_edges[:bin_number])/2

plt.figure(figsize=(10,8))
plt.step(bins, N3, color="green", label = '$z_t-z_{MLPL}$', where='mid')
plt.legend(fontsize=20, frameon=False)
plt.xlabel("Difference [mm]", fontsize=20)
plt.ylabel("Normalised number of Events", fontsize=20)
plt.ylim(0,)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress",rlabel='', fontsize=20)
plt.savefig("PVz/z_diff.png", format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

# z Difference normalised

bin_min = -5
bin_max = 5
bin_number = 30

N3, bin_edges = np.histogram((truth_z_arr-z_MLPL_arr)/sig_MLPL_arr, bins=bin_number, range=(bin_min,bin_max), density=True)
N4, bin_edges = np.histogram(np.random.normal(0,1,10000), bins=bin_number, range=(bin_min,bin_max), density=True)
bins = (bin_edges[1:]+bin_edges[:bin_number])/2

plt.figure(figsize=(10,8))
plt.step(bins, N3, color="red", label = '$(z_t-z_{MLPL})/\sigma_{MLPL}$', where='mid')
plt.step(bins, N4, color="green", label = '$G(\mu=0,\sigma=1)$', where='mid')
plt.legend(fontsize=20, frameon=False)
plt.xlabel("Difference/Uncertainty", fontsize=20)
plt.ylabel("Normalised number of Events", fontsize=20)
plt.ylim(0,)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress",rlabel='', fontsize=20)
plt.savefig("PVz/z_diff_norm.png", format='png', bbox_inches='tight')
plt.close()

########################################################################################################################################################

# Sigma over z plot

bin_min = -150
bin_max = 150
bin_number = 40
width = (bin_max-bin_min)/bin_number


N1, bin_edges = np.histogram(truth_z_arr, bins=bin_number, range=(bin_min,bin_max))
N2 = np.zeros_like(N1, dtype=float)
N3 = np.zeros_like(N1, dtype=float)
bins = (bin_edges[1:]+bin_edges[:bin_number])/2

for i in range(bin_number):
    if np.sum(sig_MLPL_arr[(truth_z_arr > (bin_min + i*width)) & (truth_z_arr < (bin_min + (i+1)*width))]) > 0:
        N2[i] += np.mean(sig_MLPL_arr[(truth_z_arr > (bin_min + i*width)) & (truth_z_arr < (bin_min + (i+1)*width))])
    if np.sum(sig_MLPL_arr[(z_MLPL_arr > (bin_min + i*width)) & (z_MLPL_arr < (bin_min + (i+1)*width))]) > 0:
        N3[i] += np.mean(sig_MLPL_arr[(z_MLPL_arr > (bin_min + i*width)) & (z_MLPL_arr < (bin_min + (i+1)*width))])


plt.figure(figsize=(12,8))
plt.step(bins, N2, color="blue", label = 'Average $\sigma_{MLPL}$ over $z_t$', where='mid')
plt.step(bins, N3, color="red", label = 'Average $\sigma_{MLPL}$ over $z_{MLPL}$', where='mid')
plt.legend(fontsize=20, frameon=False, loc='center')
plt.title("Average $\sigma_{MLPL}$ over truth and predicted $z$ value", fontsize=20)
plt.xlabel("$z$ [mm]", fontsize=20)
plt.ylabel("Average $\sigma_{MLPL}$ [mm]", fontsize=20)
plt.xlim(bin_min+0.1,bin_max-0.1)
plt.ylim(0)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
hep.atlas.label(loc=4, data=True, label="Work in Progress",rlabel='$\overline{\sigma}$='+"{:0.2f}".format(np.mean(sig_MLPL_arr)), fontsize=20)
plt.savefig("PVz/av_sigma_over_z.png", format='png', bbox_inches='tight')
plt.close()


########################################################################################################################################################

print("Evaluation successful.")