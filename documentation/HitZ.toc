\babel@toc {UKenglish}{}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}The HitZ project}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}HitZ Architecture}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Input Variables}{6}{section.2.2}%
\contentsline {chapter}{\numberline {3}Jet-by-Jet Performance}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}Jet-by-Jet PV$z$ Predictions}{8}{section.3.1}%
\contentsline {section}{\numberline {3.2}Observable Dependency}{8}{section.3.2}%
\contentsline {chapter}{\numberline {4}Pileup Rejection with HitZ}{11}{chapter.4}%
\contentsline {section}{\numberline {4.1}Pileup Rejection Theory}{11}{section.4.1}%
\contentsline {section}{\numberline {4.2}Pileup Rejection Results}{13}{section.4.2}%
\contentsline {section}{\numberline {4.3}Including L1-Trigger Chains}{14}{section.4.3}%
\contentsline {chapter}{\numberline {5}Further Applications of Hit-level Information}{16}{chapter.5}%
\contentsline {section}{\numberline {5.1}Fast ROI-tracking}{16}{section.5.1}%
\contentsline {section}{\numberline {5.2}$b$-tagging}{17}{section.5.2}%
\contentsline {chapter}{\numberline {6}Conclusion}{19}{chapter.6}%
\contentsline {part}{Appendices}{22}{part*.17}%
\contentsline {chapter}{\numberline {A}Network Parameters}{22}{appendix.A}%
\contentsline {chapter}{\numberline {B}Calculation of $jab$-coordinates}{24}{appendix.B}%
\contentsline {chapter}{\numberline {C}$\eta $-Dependency}{25}{appendix.C}%
\contentsline {chapter}{\numberline {D}Monte Carlo Reweighting}{26}{appendix.D}%
\contentsline {chapter}{\numberline {E}Athena Implementation}{27}{appendix.E}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
