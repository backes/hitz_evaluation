############################################################################

# Required packages

import h5py
import numpy as np
import math
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt
from matplotlib import colors
import itertools

############################################################################

# Loading data

base_path = "Dipz_20240126-T175203" #epoch 113
#Dipz_20240125-T164256

network_1 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/DiHiggs_withb_test.h5" #"/ckpts/DiHiggs_test.h5" 
}
network_2 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/JZ0_test.h5" 
}
network_3 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/JZ1_test.h5" 
}
network_4 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/JZ2_test.h5" 
}
network_5 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/JZ3_test.h5" 
}
network_6 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/" + base_path + "/ckpts/JZ4_test.h5" 
}


path1 = '/eos/user/b/backes/QT/preprocessing/mo_files/user.backes.'

path2 = '.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_emtopo_data.24_0_16.r12684_r12782.v1-208-gc25c7c3_output.h5/vds/vds.h5'

test_path_1 = path1 + '601479.e8514_e8528_s4159_s4114_r14774_r14754.tdd.trigger_dipz_new.24_0_21.r12684_r12782.v1-220-g5a6fb5b_output.h5/user.backes.37176216._000001.output.h5'
# test_path_1 = path1 + '601479' + path2

test_path_2 = path1 + '801165' + path2 

test_path_3 = path1 + '801166' + path2 

test_path_4 = path1 + '801167' + path2 

test_path_5 = path1 + '801168' + path2 

test_path_6 = path1 + '801169' + path2

reference = "Mo_Dipz"


print("\n====================== \n" + "  " + reference + " ROC curves \n" + "====================== \n")

############################################################################

# Load test data

num_jets = 1000000

# Signal

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets'][:num_jets]
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eta_sig = jets_sig['eta']
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']
    dipz_pred_z_sig = jets_sig['dipz20230223_z']*50.
    dipz_pred_std_sig = np.sqrt(np.exp(-1*jets_sig['dipz20230223_negLogSigma2']))*50.
    HadronConeExclTruthLabelID_sig = jets_sig['HadronConeExclTruthLabelID']
for key, val in network_1.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

# Background JZ slices

with h5py.File(test_path_2, 'r') as test_f:
    jets_jz0 = test_f['jets'][:num_jets]
    pt_MeV_jz0 = jets_jz0['pt']
    pt_jz0 = pt_MeV_jz0/1000
    eta_jz0 = jets_jz0['eta']
    eventNumber_jz0 = jets_jz0['eventNumber']
    mc_weight_jz0 = jets_jz0['mcEventWeight']
    dipz_pred_z_jz0 = jets_jz0['dipz20230223_z']*50.
    dipz_pred_std_jz0 = np.sqrt(np.exp(-1*jets_jz0['dipz20230223_negLogSigma2']))*50.
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz0 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz0 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_3, 'r') as test_f:
    jets_jz1 = test_f['jets'][:num_jets]
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eta_jz1 = jets_jz1['eta']
    eventNumber_jz1 = jets_jz1['eventNumber']
    mc_weight_jz1 = jets_jz1['mcEventWeight']
    dipz_pred_z_jz1 = jets_jz1['dipz20230223_z']*50.
    dipz_pred_std_jz1 = np.sqrt(np.exp(-1*jets_jz1['dipz20230223_negLogSigma2']))*50.
for key, val in network_3.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz1 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz1 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_4, 'r') as test_f:
    jets_jz2 = test_f['jets'][:num_jets]
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eta_jz2 = jets_jz2['eta']
    eventNumber_jz2 = jets_jz2['eventNumber']
    mc_weight_jz2 = jets_jz2['mcEventWeight']
    dipz_pred_z_jz2 = jets_jz2['dipz20230223_z']*50.
    dipz_pred_std_jz2 = np.sqrt(np.exp(-1*jets_jz2['dipz20230223_negLogSigma2']))*50.
for key, val in network_4.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz2 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz2 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_5, 'r') as test_f:
    jets_jz3 = test_f['jets'][:num_jets]
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eta_jz3 = jets_jz3['eta']
    eventNumber_jz3 = jets_jz3['eventNumber']
    mc_weight_jz3 = jets_jz3['mcEventWeight']
    dipz_pred_z_jz3 = jets_jz3['dipz20230223_z']*50.
    dipz_pred_std_jz3 = np.sqrt(np.exp(-1*jets_jz3['dipz20230223_negLogSigma2']))*50.
for key, val in network_5.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz3 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz3 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))

with h5py.File(test_path_6, 'r') as test_f:
    jets_jz4 = test_f['jets'][:num_jets]
    pt_MeV_jz4 = jets_jz4['pt']
    pt_jz4 = pt_MeV_jz4/1000
    eta_jz4 = jets_jz4['eta']
    eventNumber_jz4 = jets_jz4['eventNumber']
    mc_weight_jz4 = jets_jz4['mcEventWeight']
    dipz_pred_z_jz4 = jets_jz4['dipz20230223_z']*50.
    dipz_pred_std_jz4 = np.sqrt(np.exp(-1*jets_jz4['dipz20230223_negLogSigma2']))*50.
for key, val in network_6.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        jet_z_pred_jz4 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz4 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


############################################################################

# Trivialise weights

# mc_weight_sig = np.ones_like(mc_weight_sig, dtype=float)
# mc_weight_jz0 = np.ones_like(mc_weight_jz0, dtype=float)
# mc_weight_jz1 = np.ones_like(mc_weight_jz1, dtype=float)
# mc_weight_jz2 = np.ones_like(mc_weight_jz2, dtype=float)
# mc_weight_jz3 = np.ones_like(mc_weight_jz3, dtype=float)
# mc_weight_jz4 = np.ones_like(mc_weight_jz4, dtype=float)


# mc_weight_sig = np.zeros_like(mc_weight_sig, dtype=float)
# mc_weight_jz0 = np.zeros_like(mc_weight_jz0, dtype=float)
# mc_weight_jz1 = np.zeros_like(mc_weight_jz1, dtype=float)
# mc_weight_jz2 = np.zeros_like(mc_weight_jz2, dtype=float)
# mc_weight_jz3 = np.zeros_like(mc_weight_jz3, dtype=float)
# mc_weight_jz4 = np.zeros_like(mc_weight_jz4, dtype=float)
        
############################################################################

# Include 4b condition
        
count = 0
sum_counter = 0
for id in np.unique(eventNumber_sig):
    jets = HadronConeExclTruthLabelID_sig[eventNumber_sig == id]
    bjets = jets[jets == 5]
    if len(eventNumber_sig[eventNumber_sig == id]) < 4:
        sum_counter += 1
    if len(bjets) < 4 :
        mc_weight_sig[eventNumber_sig == id] = 0.
        count += 1

print("4-jet-condition done. Threw away ", 100*sum_counter/len(np.unique(eventNumber_sig)), "%")
print("b-condition done. Threw away ", 100*count/len(np.unique(eventNumber_sig)), "%")

############################################################################

# MC reweighting parameters

cs_jz0 = 78580000000.0 *1e-12 
filter_jz0 = 9.736785E-01 
cs_jz1 = 93901000000.0  *1e-12 
filter_jz1 = 3.513696E-02
cs_jz2 = 2582600000.0  *1e-12
filter_jz2 = 1.006522E-02 
cs_jz3 = 28528000.0  *1e-12  
filter_jz3 = 1.190844E-02 
cs_jz4 = 280140.0  *1e-12 
filter_jz4 = 1.375122E-02

total_events_sig = np.unique(eventNumber_sig).shape[0]

total_events_jz0 = np.unique(eventNumber_jz0).shape[0]
total_events_jz1 = np.unique(eventNumber_jz1).shape[0]
total_events_jz2 = np.unique(eventNumber_jz2).shape[0]
total_events_jz3 = np.unique(eventNumber_jz3).shape[0]
total_events_jz4 = np.unique(eventNumber_jz4).shape[0]

print("Number of JZ0 events: ", total_events_jz0)
print("Number of JZ1 events: ", total_events_jz1)
print("Number of JZ2 events: ", total_events_jz2)
print("Number of JZ3 events: ", total_events_jz3)
print("Number of JZ4 events: ", total_events_jz4)


# if weighted with 1/sum(weights)

_, unique_events_jz0 = np.unique(eventNumber_jz0, return_index=True)
total_events_jz0 = np.sum(mc_weight_jz0[unique_events_jz0])
_, unique_events_jz1 = np.unique(eventNumber_jz1, return_index=True)
total_events_jz1 = np.sum(mc_weight_jz1[unique_events_jz1])
_, unique_events_jz2 = np.unique(eventNumber_jz2, return_index=True)
total_events_jz2 = np.sum(mc_weight_jz2[unique_events_jz2])
_, unique_events_jz3 = np.unique(eventNumber_jz3, return_index=True)
total_events_jz3 = np.sum(mc_weight_jz3[unique_events_jz3])
_, unique_events_jz4 = np.unique(eventNumber_jz4, return_index=True)
total_events_jz4 = np.sum(mc_weight_jz4[unique_events_jz4])


weight_jz0 = cs_jz0 * filter_jz0 / total_events_jz0 *50000
weight_jz1 = cs_jz1 * filter_jz1 / total_events_jz1 *50000
weight_jz2 = cs_jz2 * filter_jz2 / total_events_jz2 *50000
weight_jz3 = cs_jz3 * filter_jz3 / total_events_jz3 *50000
weight_jz4 = cs_jz4 * filter_jz4 / total_events_jz4 *50000


############################################################################

# 2D plot to check that the predictions are similar

plt.figure(figsize=(12,8))
h = plt.hist2d(jet_z_pred_sig[:,0], dipz_pred_z_sig, bins=30, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("Own predicted position $z$", fontsize=20)
plt.ylabel("Mo's Dipz predicted position $z$", fontsize=20)
plt.xlim(-150,150)
plt.ylim(-150,150)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [-150,150]
plt.plot(x,x,c='red')
plt.savefig(reference + "/2D_jet_z_signal.png", format='png', bbox_inches='tight')
plt.close()

############################################################################

# Implementation of MLPL calculation

def calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std =std[:m]

    L_max = -9999999999.
    all_jets = range(m)
    for subset in itertools.combinations(all_jets, n):
        f = np.vectorize(math.log)
        log_l = -0.5*math.log(2*math.pi)*n
        log_l += -np.sum(f(std[[subset]]), dtype=float)
        term1 = np.sum(np.divide(pred[[subset]], std[[subset]]**2, dtype=float), dtype=float)
        term2 = np.sum(np.divide(np.ones_like(std[[subset]],dtype=float), std[[subset]]**2, dtype=float), dtype=float)
        log_l += -np.sum( np.divide((term1/term2 - pred[[subset]])**2,(2.*std[[subset]]**2), dtype=float))
        if log_l > L_max:
            L_max = log_l  
    return L_max


def multiple_jet_roc_efficiencies_new(x, n, m, cur_eN, cur_pt, cur_pred, cur_std, cur_mc_weight):

    passed_events = 0.
    correct_events = np.zeros_like(x, dtype=float)
    cur_MLPL = np.array([])
    cur_MLPL_weights = np.array([])
    
    for ev_num in np.unique(cur_eN):
          
        jets = cur_eN[cur_eN==ev_num].shape[0]

        if m == "All":
            if jets >= n:
                passed_events += cur_mc_weight[cur_eN==ev_num][0]

                l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
                cur_MLPL = np.append(cur_MLPL, l_max)
                cur_MLPL_weights = np.append(cur_MLPL_weights, cur_mc_weight[cur_eN==ev_num][0])
                correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]
                
        else:
            if jets >= m: 
                passed_events += cur_mc_weight[cur_eN==ev_num][0]
                l_max = calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std)
                correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]

    return passed_events, correct_events, cur_MLPL, cur_MLPL_weights

############################################################################

# Execution for several values of n and m

n_list = [4]
m_list = ["All"] 

xmin = 0.95
xmax = 1.
ymin = 0.8
ymax = 1e3

print("Own performance")

x = np.linspace(-50, 40, 200)
sig =[]
bkg =[]
tuple_entry = []

for n in n_list:
    print("n = ", n)
    for m in m_list:
        if m != "All":
            if m < n:
                continue
        print("m = ", m)

        passed_events_sig, correct_events_sig, MLPL_sig, MLPL_sig_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig, mc_weight_sig)

        passed_events_jz0, correct_events_jz0, cur_MLPL_jz0, cur_MLPL_jz0_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz0, pt_jz0, jet_z_pred_jz0, jet_z_stddev_jz0, mc_weight_jz0)

        passed_events_jz1, correct_events_jz1, cur_MLPL_jz1, cur_MLPL_jz1_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz1, pt_jz1, jet_z_pred_jz1, jet_z_stddev_jz1, mc_weight_jz1)
        
        passed_events_jz2, correct_events_jz2, cur_MLPL_jz2, cur_MLPL_jz2_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz2, pt_jz2, jet_z_pred_jz2, jet_z_stddev_jz2, mc_weight_jz2)
        
        passed_events_jz3, correct_events_jz3, cur_MLPL_jz3, cur_MLPL_jz3_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz3, pt_jz3, jet_z_pred_jz3, jet_z_stddev_jz3, mc_weight_jz3)
       
        passed_events_jz4, correct_events_jz4, cur_MLPL_jz4, cur_MLPL_jz4_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz4, pt_jz4, jet_z_pred_jz4, jet_z_stddev_jz4, mc_weight_jz4)


        eff_sig = correct_events_sig / passed_events_sig

        correct_events_bkg = (correct_events_jz0 * weight_jz0 #* passed_events_jz0/total_events_jz0
                   + correct_events_jz1 * weight_jz1 #* passed_events_jz1/total_events_jz1
                   + correct_events_jz2 * weight_jz2 #* passed_events_jz2/total_events_jz2
                   + correct_events_jz3 * weight_jz3 #* passed_events_jz3/total_events_jz3
                   + correct_events_jz4 * weight_jz4) #* passed_events_jz4/total_events_jz4 )
        
        passed_events_bkg = (passed_events_jz0 * weight_jz0 #* passed_events_jz0/total_events_jz0
                   + passed_events_jz1 * weight_jz1 #* passed_events_jz1/total_events_jz1
                   + passed_events_jz2 * weight_jz2 #* passed_events_jz2/total_events_jz2
                   + passed_events_jz3 * weight_jz3 #* passed_events_jz3/total_events_jz3
                   + passed_events_jz4 * weight_jz4 )#* passed_events_jz4/total_events_jz4) 

        correct_events_bkg[correct_events_bkg==0] = np.nan
        eff_bkg = correct_events_bkg / passed_events_bkg
        rej_bkg = 1./eff_bkg
        
        
        x_ref = np.linspace(0.01,1,10000)
        plt.figure(figsize=(12,8))
        plt.plot(eff_sig, rej_bkg, marker='o', color='blue', label="MLPL(n={},m={})".format(n,m))
        plt.plot(x_ref, 1/x_ref, linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
        plt.legend(fontsize=20, frameon=False)
        plt.title("ROC Curve", fontsize=20)
        plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
        plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
        plt.xlim(xmin, xmax)
        plt.yscale('log')
        plt.ylim(ymin,ymax)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(True, which="both")
        plt.savefig(reference + "/" + "Own_Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
        plt.close()

        sig.append(eff_sig)
        bkg.append(rej_bkg)
        tuple_entry.append((n,m))

        if m == "All":
            MLPL_bkg = np.concatenate((cur_MLPL_jz0, cur_MLPL_jz1, cur_MLPL_jz2, cur_MLPL_jz3, cur_MLPL_jz4))
            MLPL_bkg_weights = np.concatenate((cur_MLPL_jz0_weights*weight_jz0,
                                                cur_MLPL_jz1_weights*weight_jz1,
                                                  cur_MLPL_jz2_weights*weight_jz2,
                                                    cur_MLPL_jz3_weights*weight_jz3,
                                                      cur_MLPL_jz4_weights*weight_jz4))            
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-40,20), weights=MLPL_sig_weights, density=True)
            mlplS_uw, bins = np.histogram(MLPL_sig, bins=100, range=(-40,20), density=True)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-40,20), weights=MLPL_bkg_weights, density=True)
            mlplB_uw, bins = np.histogram(MLPL_bkg, bins=100, range=(-40,20), density=True)            


            plt.figure(figsize=(12,8))
            plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
            plt.step(bins[1:], mlplS_uw, c='orange', label="MLPL(4,All) Signal without reweighting")
            plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Background")
            plt.step(bins[1:], mlplB_uw, c='green', label="MLPL(4,All) Background without reweighting")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlabel("MLPL(4, All)", fontsize=20)
            plt.ylabel("Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            plt.savefig(reference + "/" + "Own_MLPL.png", format='png', bbox_inches='tight')
            plt.close()

            MLPL0, bins = np.histogram(cur_MLPL_jz0, bins=100, range=(-20,0), weights=cur_MLPL_jz0_weights, density=True)
            MLPL1, bins = np.histogram(cur_MLPL_jz1, bins=100, range=(-20,0), weights=cur_MLPL_jz1_weights, density=True)
            MLPL2, bins = np.histogram(cur_MLPL_jz2, bins=100, range=(-20,0), weights=cur_MLPL_jz2_weights, density=True)
            MLPL3, bins = np.histogram(cur_MLPL_jz3, bins=100, range=(-20,0), weights=cur_MLPL_jz3_weights, density=True)
            MLPL4, bins = np.histogram(cur_MLPL_jz4, bins=100, range=(-20,0), weights=cur_MLPL_jz4_weights, density=True)

            plt.figure(figsize=(12,8))
            plt.step(bins[1:], MLPL0, c='blue', label="MLPL(4,All) of JZ0")
            plt.step(bins[1:], MLPL1, c='orange', label="MLPL(4,All) of JZ1")
            plt.step(bins[1:], MLPL2, c='green', label="MLPL(4,All) of JZ2")
            plt.step(bins[1:], MLPL3, c='red', label="MLPL(4,All) of JZ3")
            plt.step(bins[1:], MLPL4, c='violet', label="MLPL(4,All) of JZ4")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlabel("MLPL(4,All)", fontsize=20)
            plt.ylabel("Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            plt.savefig(reference + "/" + "Own_individual_MLPL.png", format='png', bbox_inches='tight')
            plt.close()


x_ref = np.linspace(0.01, 1, 10000)
color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

plt.figure(figsize=(12,8))
for l, entry in enumerate(tuple_entry):
    c = next(color)
    plt.plot(sig[l], bkg[l], marker='o', color=c, label="MLPL{}".format(entry))
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.title("ROC Curves", fontsize=20)
plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(xmin, xmax)
plt.ylim(ymin,ymax)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.grid(True, which="both")
plt.savefig(reference + "/" + "Own_Roc_total.png" , format='png', bbox_inches='tight')
plt.close()

############################################################################

print("Mo's performance")

x = np.concatenate((np.array([-10000]), np.linspace(-100, 40, 2000)))
sig =[]
bkg =[]
tuple_entry = []

for n in n_list:
    print("n = ", n)
    for m in m_list:
        if m != "All":
            if m < n:
                continue
        print("m = ", m)
        
        print("sig")
        passed_events_sig, correct_events_sig, MLPL_sig, MLPL_sig_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig, mc_weight_sig)

        print("bkg")
        passed_events_jz0, correct_events_jz0, cur_MLPL_jz0, cur_MLPL_jz0_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz0, pt_jz0, dipz_pred_z_jz0, dipz_pred_std_jz0, mc_weight_jz0)

        passed_events_jz1, correct_events_jz1, cur_MLPL_jz1, cur_MLPL_jz1_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz1, pt_jz1, dipz_pred_z_jz1, dipz_pred_std_jz1, mc_weight_jz1)
        
        passed_events_jz2, correct_events_jz2, cur_MLPL_jz2, cur_MLPL_jz2_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz2, pt_jz2, dipz_pred_z_jz2, dipz_pred_std_jz2, mc_weight_jz2)
        
        passed_events_jz3, correct_events_jz3, cur_MLPL_jz3, cur_MLPL_jz3_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz3, pt_jz3, dipz_pred_z_jz3, dipz_pred_std_jz3, mc_weight_jz3)
       
        passed_events_jz4, correct_events_jz4, cur_MLPL_jz4, cur_MLPL_jz4_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz4, pt_jz4, dipz_pred_z_jz4, dipz_pred_std_jz4, mc_weight_jz4)

        eff_sig = correct_events_sig / passed_events_sig
        
        print(x[eff_sig>0.98])

        correct_events_bkg = (correct_events_jz0 * weight_jz0 #* passed_events_jz0/total_events_jz0
                   + correct_events_jz1 * weight_jz1 #* passed_events_jz1/total_events_jz1
                   + correct_events_jz2 * weight_jz2 #* passed_events_jz2/total_events_jz2
                   + correct_events_jz3 * weight_jz3 #* passed_events_jz3/total_events_jz3
                   + correct_events_jz4 * weight_jz4 )#* passed_events_jz4/total_events_jz4 )
        
        passed_events_bkg = (passed_events_jz0 * weight_jz0 #* passed_events_jz0/total_events_jz0
                   + passed_events_jz1 * weight_jz1 #* passed_events_jz1/total_events_jz1
                   + passed_events_jz2 * weight_jz2 #* passed_events_jz2/total_events_jz2
                   + passed_events_jz3 * weight_jz3 #* passed_events_jz3/total_events_jz3
                   + passed_events_jz4 * weight_jz4 )#* passed_events_jz4/total_events_jz4)  

        correct_events_bkg[correct_events_bkg==0] = np.nan
        eff_bkg = correct_events_bkg / passed_events_bkg
        rej_bkg = 1./eff_bkg
        
        x_ref = np.linspace(0.01,1,10000)
        plt.figure(figsize=(12,8))
        plt.plot(eff_sig, rej_bkg, marker='o', color='blue', label="MLPL(n={},m={})".format(n,m))
        plt.plot(x_ref, 1/x_ref, linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
        plt.legend(fontsize=20, frameon=False)
        plt.title("ROC Curve", fontsize=20)
        plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
        plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
        plt.xlim(xmin, xmax)
        plt.ylim(ymin,ymax)
        plt.yscale('log')
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(True, which="both")
        plt.savefig(reference + "/" + "Mo_Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
        plt.close()

        sig.append(eff_sig)
        bkg.append(rej_bkg)
        tuple_entry.append((n,m))

        if m == "All":
            MLPL_bkg = np.concatenate((cur_MLPL_jz0, cur_MLPL_jz1, cur_MLPL_jz2, cur_MLPL_jz3, cur_MLPL_jz4))
            MLPL_bkg_weights = np.concatenate((cur_MLPL_jz0_weights*weight_jz0 ,#* passed_events_jz0/total_events_jz0,
                                                cur_MLPL_jz1_weights*weight_jz1 ,#* passed_events_jz1/total_events_jz1,
                                                  cur_MLPL_jz2_weights*weight_jz2 ,#* passed_events_jz2/total_events_jz2,
                                                    cur_MLPL_jz3_weights*weight_jz3 ,#* passed_events_jz3/total_events_jz3,
                                                      cur_MLPL_jz4_weights*weight_jz4 ))#* passed_events_jz4/total_events_jz4))             
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-40,20), weights=MLPL_sig_weights, density=True)
            mlplS_uw, bins = np.histogram(MLPL_sig, bins=100, range=(-40,20), density=True)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-40,20), weights=MLPL_bkg_weights, density=True)
            mlplB_uw, bins = np.histogram(MLPL_bkg, bins=100, range=(-40,20), density=True)            

            plt.figure(figsize=(12,8))
            plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
            plt.step(bins[1:], mlplS_uw, c='orange', label="MLPL(4,All) Signal without reweighting")
            plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Background")
            plt.step(bins[1:], mlplB_uw, c='green', label="MLPL(4,All) Background without reweighting")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlabel("MLPL(4, All)", fontsize=20)
            plt.ylabel("Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            plt.savefig(reference + "/" + "Mo_MLPL.png", format='png', bbox_inches='tight')
            plt.close()

            MLPL0, bins = np.histogram(cur_MLPL_jz0, bins=100, range=(-20,0), weights=cur_MLPL_jz0_weights, density=True)
            MLPL1, bins = np.histogram(cur_MLPL_jz1, bins=100, range=(-20,0), weights=cur_MLPL_jz1_weights, density=True)
            MLPL2, bins = np.histogram(cur_MLPL_jz2, bins=100, range=(-20,0), weights=cur_MLPL_jz2_weights, density=True)
            MLPL3, bins = np.histogram(cur_MLPL_jz3, bins=100, range=(-20,0), weights=cur_MLPL_jz3_weights, density=True)
            MLPL4, bins = np.histogram(cur_MLPL_jz4, bins=100, range=(-20,0), weights=cur_MLPL_jz4_weights, density=True)

            plt.figure(figsize=(12,8))
            plt.step(bins[1:], MLPL0, c='blue', label="MLPL(4,All) of JZ0")
            plt.step(bins[1:], MLPL1, c='orange', label="MLPL(4,All) of JZ1")
            plt.step(bins[1:], MLPL2, c='green', label="MLPL(4,All) of JZ2")
            plt.step(bins[1:], MLPL3, c='red', label="MLPL(4,All) of JZ3")
            plt.step(bins[1:], MLPL4, c='violet', label="MLPL(4,All) of JZ4")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlabel("MLPL(4,All)", fontsize=20)
            plt.ylabel("Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            plt.savefig(reference + "/" + "Mo_individual_MLPL.png", format='png', bbox_inches='tight')
            plt.close()


#####################################################################################################################################################################
            
            # Calculate Stuff with Mo's algorithm

            #Mo corrected

            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-20,0), weights=MLPL_sig_weights)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-20,0), weights=MLPL_bkg_weights)

            sigeff_Mo_1 = ( passed_events_sig - MLPL_sig_weights[MLPL_sig<-20].sum() - mlplS[:].cumsum() ) / passed_events_sig
            bckgeff_Mo_1 = (passed_events_bkg - MLPL_bkg_weights[MLPL_bkg<-20].sum() - mlplB[:].cumsum() ) / passed_events_bkg


            #Mo's original
            total_events_jz0 = np.unique(eventNumber_jz0).shape[0]
            total_events_jz1 = np.unique(eventNumber_jz1).shape[0]
            total_events_jz2 = np.unique(eventNumber_jz2).shape[0]
            total_events_jz3 = np.unique(eventNumber_jz3).shape[0]
            total_events_jz4 = np.unique(eventNumber_jz4).shape[0]

            weight_jz0 = cs_jz0 * filter_jz0 / total_events_jz0 *50000
            weight_jz1 = cs_jz1 * filter_jz1 / total_events_jz1 *50000
            weight_jz2 = cs_jz2 * filter_jz2 / total_events_jz2 *50000
            weight_jz3 = cs_jz3 * filter_jz3 / total_events_jz3 *50000
            weight_jz4 = cs_jz4 * filter_jz4 / total_events_jz4 *50000

            MLPL_bkg = np.concatenate((cur_MLPL_jz0, cur_MLPL_jz1, cur_MLPL_jz2, cur_MLPL_jz3, cur_MLPL_jz4))
            MLPL_bkg_weights = np.concatenate((np.ones_like(cur_MLPL_jz0_weights, dtype=float)*weight_jz0 ,#* passed_events_jz0/total_events_jz0,
                                                np.ones_like(cur_MLPL_jz1_weights, dtype=float)*weight_jz1 ,#* passed_events_jz1/total_events_jz1,
                                                  np.ones_like(cur_MLPL_jz2_weights, dtype=float)*weight_jz2 ,#* passed_events_jz2/total_events_jz2,
                                                    np.ones_like(cur_MLPL_jz3_weights, dtype=float)*weight_jz3 ,#* passed_events_jz3/total_events_jz3,
                                                      np.ones_like(cur_MLPL_jz4_weights, dtype=float)*weight_jz4 ))#* passed_events_jz4/total_events_jz4))              
            sig_weights_copied = MLPL_sig_weights
            sig_weights_copied[MLPL_sig_weights != 0.] = 1.
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-20,0), weights=sig_weights_copied)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-20,0), weights=MLPL_bkg_weights)

            sigeff_Mo_2 = ( mlplS.sum() - mlplS[:].cumsum() ) / mlplS.sum()
    
            bckgeff_Mo_2 = ( mlplB.sum() - mlplB[:].cumsum() ) / mlplB.sum()
            
            rej_Mo_1 = 1./bckgeff_Mo_1
            rej_Mo_2 = 1./bckgeff_Mo_2


#####################################################################################################################################################################


x_ref = np.linspace(0.01, 1, 10000)
color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

plt.figure(figsize=(12,8))
for l, entry in enumerate(tuple_entry):
    c = next(color)
    plt.plot(sig[l], bkg[l], marker='o', color=c, label="MLPL{}".format(entry))

plt.plot(sigeff_Mo_1, rej_Mo_1, marker='o', color='blue', label = 'Mo MLPL(4,All) corrected')
plt.plot(sigeff_Mo_2, rej_Mo_2, marker='o', color='green', label = 'Mo MLPL(4,All) uncorrected')

plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.title("ROC Curves", fontsize=20)
plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.grid(True, which="both")
plt.savefig(reference + "/" + "Mo_Roc_total.png" , format='png', bbox_inches='tight')
plt.close()

############################################################################
print("Plotting successful.")