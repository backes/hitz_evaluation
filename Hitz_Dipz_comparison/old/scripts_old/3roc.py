############################################################################

# Required packages

import h5py
import numpy as np
import math
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt
from matplotlib import colors
import itertools

############################################################################


# DipZ or HitZ


# network_1 = {
#     "Hitz" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_20231216-T154456/ckpts/epoch=068-val_loss=-1.73468__test_ttbar.h5"
# }
# test_path_1 = '/eos/user/b/backes/QT/preprocessing/Hitz_new/pp_output_test_ttbar.h5'

# network_2 = {
#     "Hitz" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_20231216-T154456/ckpts/epoch=068-val_loss=-1.73468__test_jj.h5"
# }
# test_path_2 = '/eos/user/b/backes/QT/preprocessing/Hitz_new/pp_output_test_jj.h5'

# reference = "Hitz"


network_1 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_ttbar.h5" 
}
test_path_1 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_ttbar.h5'


network_2 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz0.h5"
}
test_path_2 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz0.h5'


network_3 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz1.h5"
}
test_path_3 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz1.h5'

network_4 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz2.h5"
}
test_path_4 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz2.h5'


network_5 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz3.h5"
}
test_path_5 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz3.h5'


network_6 = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20240115-T093750/ckpts/epoch=024-val_loss=-2.20046__test_jz4.h5"
}
test_path_6 = '/eos/user/b/backes/QT/preprocessing/Dipz_all_JZ/pp_output_test_jz4.h5'


reference = "Dipz"



print("\n=================== \n" + "  " + reference + " ROC curves \n" + "=================== \n")

############################################################################

# Load test data

# Signal

with h5py.File(test_path_1, 'r') as test_f:
    jets_sig = test_f['jets']
    jet_z_sig = jets_sig['TruthJetPVz']
    pt_MeV_sig = jets_sig['pt']
    pt_sig = pt_MeV_sig/1000
    eta_sig = jets_sig['eta']
    eventNumber_sig = jets_sig['eventNumber']
    mc_weight_sig = jets_sig['mcEventWeight']

for key, val in network_1.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets']
        jet_z_pred_sig = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_sig = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


# Background JZ slices

with h5py.File(test_path_2, 'r') as test_f:
    jets_jz0 = test_f['jets']
    jet_z_jz0 = jets_jz0['TruthJetPVz']
    pt_MeV_jz0 = jets_jz0['pt']
    pt_jz0 = pt_MeV_jz0/1000
    eta_jz0 = jets_jz0['eta']
    eventNumber_jz0 = jets_jz0['eventNumber']
    mc_weight_jz0 = jets_jz0['mcEventWeight']
for key, val in network_2.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets']
        jet_z_pred_jz0 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz0 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


with h5py.File(test_path_3, 'r') as test_f:
    jets_jz1 = test_f['jets']
    jet_z_jz1 = jets_jz1['TruthJetPVz']
    pt_MeV_jz1 = jets_jz1['pt']
    pt_jz1 = pt_MeV_jz1/1000
    eta_jz1 = jets_jz1['eta']
    eventNumber_jz1 = jets_jz1['eventNumber']
    mc_weight_jz1 = jets_jz1['mcEventWeight']
for key, val in network_3.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets']
        jet_z_pred_jz1 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz1 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


with h5py.File(test_path_4, 'r') as test_f:
    jets_jz2 = test_f['jets']
    jet_z_jz2 = jets_jz2['TruthJetPVz']
    pt_MeV_jz2 = jets_jz2['pt']
    pt_jz2 = pt_MeV_jz2/1000
    eta_jz2 = jets_jz2['eta']
    eventNumber_jz2 = jets_jz2['eventNumber']
    mc_weight_jz2 = jets_jz2['mcEventWeight']
for key, val in network_4.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets']
        jet_z_pred_jz2 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz2 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


with h5py.File(test_path_5, 'r') as test_f:
    jets_jz3 = test_f['jets']
    jet_z_jz3 = jets_jz3['TruthJetPVz']
    pt_MeV_jz3 = jets_jz3['pt']
    pt_jz3 = pt_MeV_jz3/1000
    eta_jz3 = jets_jz3['eta']
    eventNumber_jz3 = jets_jz3['eventNumber']
    mc_weight_jz3 = jets_jz3['mcEventWeight']
for key, val in network_5.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets']
        jet_z_pred_jz3 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz3 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


with h5py.File(test_path_6, 'r') as test_f:
    jets_jz4 = test_f['jets']
    jet_z_jz4 = jets_jz4['TruthJetPVz']
    pt_MeV_jz4 = jets_jz4['pt']
    pt_jz4 = pt_MeV_jz4/1000
    eta_jz4 = jets_jz4['eta']
    eventNumber_jz4 = jets_jz4['eventNumber']
    mc_weight_jz4 = jets_jz4['mcEventWeight']
for key, val in network_6.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets']
        jet_z_pred_jz4 = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev_jz4 = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))


############################################################################

# MC reweighting parameters

cs_jz0 =  78580000000.0
filter_jz0 = 9.7636785E-01
cs_jz1 = 93901000000.0 
filter_jz1 = 3.513696E-02
cs_jz2 = 2582600000.0
filter_jz2 = 1.006522E-02
cs_jz3 = 28528000.0
filter_jz3 = 1.190844E-02
cs_jz4 = 280140.0
filter_jz4 = 1.375122E-02 

total_events_sig = np.unique(eventNumber_sig).shape[0]

# AOD info

# total_events_jz0 = 50000
# total_events_jz1 = 50000
# total_events_jz2 = 50000
# total_events_jz3 = 50000
# total_events_jz4 = 50000

# if weighted with 1/sum(events)

# total_events_jz0 = np.unique(eventNumber_jz0).shape[0]
# total_events_jz1 = np.unique(eventNumber_jz1).shape[0]
# total_events_jz2 = np.unique(eventNumber_jz2).shape[0]
# total_events_jz3 = np.unique(eventNumber_jz3).shape[0]
# total_events_jz4 = np.unique(eventNumber_jz4).shape[0]

# print(total_events_jz0)

# if weighted with 1/sum(weights)

_, unique_events_jz0 = np.unique(eventNumber_jz0, return_index=True)
total_events_jz0 = np.sum(mc_weight_jz0[unique_events_jz0])
_, unique_events_jz1 = np.unique(eventNumber_jz1, return_index=True)
total_events_jz1 = np.sum(mc_weight_jz1[unique_events_jz1])
_, unique_events_jz2 = np.unique(eventNumber_jz2, return_index=True)
total_events_jz2 = np.sum(mc_weight_jz2[unique_events_jz2])
_, unique_events_jz3 = np.unique(eventNumber_jz3, return_index=True)
total_events_jz3 = np.sum(mc_weight_jz3[unique_events_jz3])
_, unique_events_jz4 = np.unique(eventNumber_jz4, return_index=True)
total_events_jz4 = np.sum(mc_weight_jz4[unique_events_jz4])


weight_jz0 = cs_jz0 * filter_jz0 / total_events_jz0 
weight_jz1 = cs_jz1 * filter_jz1 / total_events_jz1 
weight_jz2 = cs_jz2 * filter_jz2 / total_events_jz2 
weight_jz3 = cs_jz3 * filter_jz3 / total_events_jz3 
weight_jz4 = cs_jz4 * filter_jz4 / total_events_jz4 

print("Number of signal events: ", total_events_sig)
print("Number of JZ0 events: ", total_events_jz0)
print("Number of JZ1 events: ", total_events_jz1)
print("Number of JZ2 events: ", total_events_jz2)
print("Number of JZ3 events: ", total_events_jz3)
print("Number of JZ4 events: ", total_events_jz4)


############################################################################

# Signal plot

jet_z_plot = HistogramPlot(
    bins=np.linspace(-200, 200, 41),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)
jet_z_plot.add(
    Histogram(
        jet_z_sig.flatten(), label="True"
    )
)
jet_z_plot.add(
        Histogram(jet_z_pred_sig.flatten(), label=reference)
)
jet_z_plot.draw()
jet_z_plot.savefig("New_" + reference + "/jet_z_signal.png")


############################################################################

# 2D Signal Plot

plt.figure(figsize=(12,8))
h = plt.hist2d(jet_z_sig, jet_z_pred_sig.flatten(), bins=30, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("True position $z_t$", fontsize=20)
plt.ylabel("Predicted position $z_p$", fontsize=20)
plt.xlim(-150,150)
plt.ylim(-150,150)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [-150,150]
plt.plot(x,x,c='red')
plt.savefig("New_" + reference + "/2D_jet_z_signal.png", format='png', bbox_inches='tight')
plt.close()


############################################################################

# pt validation plots


bin_min = 20
bin_max = 300
bin_number = 50

pt0, bins = np.histogram(pt_jz0, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz0*weight_jz0)
pt1, bins = np.histogram(pt_jz1, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz1*weight_jz1)
pt2, bins = np.histogram(pt_jz2, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz2*weight_jz2)
pt3, bins = np.histogram(pt_jz3, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz3*weight_jz3)
pt4, bins = np.histogram(pt_jz4, bins=bin_number, range=(bin_min,bin_max), weights=mc_weight_jz4*weight_jz4)

plt.figure(figsize=(12,8))
bottom = np.zeros_like(bins[1:], dtype=float)
# pt_label = ["$p_T$ of JZ0", "$p_T$ of JZ1","$p_T$ of JZ2","$p_T$ of JZ3","$p_T$ of JZ4"]
# pt_color = ["b","orange","g","r","violet"]
# for i, pt_JZ in enumerate([pt0,pt1,pt2,pt3,pt4]):
pt_label = ["$p_T$ of JZ4", "$p_T$ of JZ3","$p_T$ of JZ2","$p_T$ of JZ1"]#,"$p_T$ of JZ0"]
pt_color = ["violet","r","g","orange"]#,"blue"]
for i, pt_JZ in enumerate([pt4,pt3,pt2,pt1]): #,pt0]):
    plt.bar((bins[1:]+bins[:-1])/2, pt_JZ, bottom=bottom, width =(bin_max-bin_min)/bin_number, label=pt_label[i], color=pt_color[i])
    bottom += pt_JZ
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("$p_T$", fontsize=20)
plt.ylabel("Number of Jets", fontsize=20)
plt.ylim(1e-0,1e12)
plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig("New_" + reference + "/" + "pt_dist.png", format='png', bbox_inches='tight')
plt.close()

plt.figure(figsize=(12,8))
# plt.step(bins[1:], pt0, c='blue', label="$p_T$ of JZ0")
plt.step(bins[1:], pt1, c='orange', label="$p_T$ of JZ1")
plt.step(bins[1:], pt2, c='green', label="$p_T$ of JZ2")
plt.step(bins[1:], pt3, c='red', label="$p_T$ of JZ3")
plt.step(bins[1:], pt4, c='violet', label="$p_T$ of JZ4")
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("$p_T$", fontsize=20)
plt.ylabel("Number of Jets", fontsize=20)
plt.ylim(1e-0,1e12)
plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig("New_" + reference + "/" + "pt_not_stacked.png", format='png', bbox_inches='tight')
plt.close()


############################################################################

def get_leading_pt(cur_pt, cur_eN, cur_mc_weight):

    pt_array = np.array([])
    weight_array = np.array([])
    evts1 =0
    for ev_num in np.unique(cur_eN):
        this_event_pt = cur_pt[cur_eN==ev_num].argsort()

        if (this_event_pt.shape[0]==1):
            evts1 +=1
        this_event_mc_weight = cur_mc_weight[cur_eN==ev_num]
        pt = cur_pt[cur_eN==ev_num]
        pt = pt[this_event_pt[::-1]]
        pt_array = np.append(pt_array, pt[0])
        weight_array = np.append(weight_array, this_event_mc_weight[0])
    print(evts1 / len(np.unique(cur_eN)))
    return pt_array, weight_array



pt_jz0_leading, weight_leading_pt_jz0 = get_leading_pt(pt_jz0, eventNumber_jz0, mc_weight_jz0)
pt_jz1_leading, weight_leading_pt_jz1 = get_leading_pt(pt_jz1, eventNumber_jz1, mc_weight_jz1)
pt_jz2_leading, weight_leading_pt_jz2 = get_leading_pt(pt_jz2, eventNumber_jz2, mc_weight_jz2)
pt_jz3_leading, weight_leading_pt_jz3 = get_leading_pt(pt_jz3, eventNumber_jz3, mc_weight_jz3)
pt_jz4_leading, weight_leading_pt_jz4 = get_leading_pt(pt_jz4, eventNumber_jz4, mc_weight_jz4)


bin_min = 20
bin_max = 300
bin_number = 50

pt0, bins = np.histogram(pt_jz0_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz0*weight_jz0)
pt1, bins = np.histogram(pt_jz1_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz1*weight_jz1)
pt2, bins = np.histogram(pt_jz2_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz2*weight_jz2)
pt3, bins = np.histogram(pt_jz3_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz3*weight_jz3)
pt4, bins = np.histogram(pt_jz4_leading, bins=bin_number, range=(bin_min,bin_max), weights=weight_leading_pt_jz4*weight_jz4)


plt.figure(figsize=(12,8))
bottom = np.zeros_like(bins[1:], dtype=float)
# pt_label = ["$p_T$ of JZ0", "$p_T$ of JZ1","$p_T$ of JZ2","$p_T$ of JZ3","$p_T$ of JZ4"]
# pt_color = ["b","orange","g","r","violet"]

pt_label = ["$p_T$ of JZ4", "$p_T$ of JZ3","$p_T$ of JZ2","$p_T$ of JZ1"]#,"$p_T$ of JZ0"]
pt_color = ["violet","r","g","orange"]#,"blue"]
for i, pt_JZ in enumerate([pt4,pt3,pt2,pt1]):#,pt0]):
    plt.bar((bins[1:]+bins[:-1])/2, pt_JZ, bottom=bottom, width =(bin_max-bin_min)/bin_number, label=pt_label[i], color=pt_color[i])
    bottom += pt_JZ
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("Leading $p_T$", fontsize=20)
plt.ylabel("Number of Events", fontsize=20)
plt.ylim(1e-0,1e12)
plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig("New_" + reference + "/" + "leading_pt_dist.png", format='png', bbox_inches='tight')
plt.close()

plt.figure(figsize=(12,8))
# plt.step(bins[1:], pt0, c='blue', label="$p_T$ of JZ0")
plt.step(bins[1:], pt1, c='orange', label="$p_T$ of JZ1")
plt.step(bins[1:], pt2, c='green', label="$p_T$ of JZ2")
plt.step(bins[1:], pt3, c='red', label="$p_T$ of JZ3")
plt.step(bins[1:], pt4, c='violet', label="$p_T$ of JZ4")
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel("Leading $p_T$", fontsize=20)
plt.ylabel("Number of Events", fontsize=20)
plt.ylim(1e-0,1e12)
plt.yscale('log')
plt.legend(fontsize=20, frameon=False)
plt.savefig("New_" + reference + "/" + "leading_pt_not_stacked.png", format='png', bbox_inches='tight')
plt.close()


############################################################################


# Implementation of MLPL calculation

def calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std):

    this_event_pt = cur_pt[cur_eN==ev_num].argsort()
    pred = cur_pred[cur_eN==ev_num]
    std = cur_std[cur_eN==ev_num]
    pred = pred[this_event_pt[::-1]]
    pred = pred[:m]
    std = std[this_event_pt[::-1]]
    std =std[:m]

    
    L_max = -1000
    all_jets = range(m)
    for subset in itertools.combinations(all_jets, n):
        f = np.vectorize(math.log)
        log_l = -0.5*math.log(2*math.pi)*n
        log_l += -np.sum(f(std[[subset]]))
        term = np.sum(pred[[subset]]/std[[subset]]**2) / np.sum(1/std[[subset]]**2)
        log_l += -np.sum(( term - pred[[subset]])**2 / (2*std[[subset]]**2))
        
        if log_l > L_max:
            L_max = log_l
            
    return L_max



def multiple_jet_roc_efficiencies_new(x, n, m, cur_eN, cur_pt, cur_pred, cur_std, cur_mc_weight):

    passed_events = 0.
    correct_events = np.zeros_like(x, dtype=float)
    cur_MLPL = np.array([])
    cur_MLPL_weights = np.array([])
    
    for ev_num in np.unique(cur_eN):
          
        jets = cur_eN[cur_eN==ev_num].shape[0]

        if m == "All":
            if jets >= n:
                passed_events += cur_mc_weight[cur_eN==ev_num][0]

                l_max = calculate_L_max(ev_num, n, jets, cur_eN, cur_pt, cur_pred, cur_std)
                cur_MLPL = np.append(cur_MLPL, l_max)
                cur_MLPL_weights = np.append(cur_MLPL_weights, cur_mc_weight[cur_eN==ev_num][0])
                correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]
        else:
            if jets >= m: 
                passed_events += cur_mc_weight[cur_eN==ev_num][0]
                l_max = calculate_L_max(ev_num, n, m, cur_eN, cur_pt, cur_pred, cur_std)
                correct_events[x<l_max] += cur_mc_weight[cur_eN==ev_num][0]

    return passed_events, correct_events, cur_MLPL, cur_MLPL_weights


############################################################################

# Execution for several values of n and m

n_list = [4]
m_list = [4,5,"All"]

x = np.linspace(-50, 40, 200)
sig =[]
bkg =[]
tuple_entry = []

for n in n_list:
    print("n = ", n)
    for m in m_list:
        if m != "All":
            if m < n:
                continue
        print("m = ", m)

        passed_events_sig, correct_events_sig, MLPL_sig, MLPL_sig_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_sig, pt_sig, jet_z_pred_sig, jet_z_stddev_sig, mc_weight_sig)

        passed_events_jz0, correct_events_jz0, cur_MLPL_jz0, cur_MLPL_jz0_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz0, pt_jz0, jet_z_pred_jz0, jet_z_stddev_jz0, mc_weight_jz0)

        passed_events_jz1, correct_events_jz1, cur_MLPL_jz1, cur_MLPL_jz1_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz1, pt_jz1, jet_z_pred_jz1, jet_z_stddev_jz1, mc_weight_jz1)
        
        passed_events_jz2, correct_events_jz2, cur_MLPL_jz2, cur_MLPL_jz2_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz2, pt_jz2, jet_z_pred_jz2, jet_z_stddev_jz2, mc_weight_jz2)
        
        passed_events_jz3, correct_events_jz3, cur_MLPL_jz3, cur_MLPL_jz3_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz3, pt_jz3, jet_z_pred_jz3, jet_z_stddev_jz3, mc_weight_jz3)
       
        passed_events_jz4, correct_events_jz4, cur_MLPL_jz4, cur_MLPL_jz4_weights = multiple_jet_roc_efficiencies_new(
            x, n, m, eventNumber_jz4, pt_jz4, jet_z_pred_jz4, jet_z_stddev_jz4, mc_weight_jz4)


        eff_sig = correct_events_sig / passed_events_sig

        correct_events_bkg = (correct_events_jz1 * weight_jz1 
                   + correct_events_jz2 * weight_jz2 
                   + correct_events_jz3 * weight_jz3 
                   + correct_events_jz4 * weight_jz4)
        
        passed_events_bkg = (passed_events_jz1 * weight_jz1 
                   + passed_events_jz2 * weight_jz2 
                   + passed_events_jz3 * weight_jz3 
                   + passed_events_jz4 * weight_jz4)
        

        # correct_events_bkg = (correct_events_jz0 * weight_jz0 
        #            + correct_events_jz1 * weight_jz1 
        #            + correct_events_jz2 * weight_jz2 
        #            + correct_events_jz3 * weight_jz3 
        #            + correct_events_jz4 * weight_jz4)
        
        # passed_events_bkg = (passed_events_jz0 * weight_jz0 
        #            + passed_events_jz1 * weight_jz1 
        #            + passed_events_jz2 * weight_jz2 
        #            + passed_events_jz3 * weight_jz3 
        #            + passed_events_jz4 * weight_jz4)

        correct_events_bkg[correct_events_bkg==0] = np.nan
        eff_bkg = correct_events_bkg / passed_events_bkg
        rej_bkg = 1./eff_bkg
        
        
        x_ref = np.linspace(0.01,1,10000)
        plt.figure(figsize=(12,8))
        plt.plot(eff_sig, rej_bkg, marker='o', color='blue', label="MLPL(n={},m={})".format(n,m))
        plt.plot(x_ref, 1/x_ref, linestyle='--', color='red', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
        plt.legend(fontsize=20, frameon=False)
        plt.title("ROC Curve", fontsize=20)
        plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
        plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
        plt.xlim(0,1)
        plt.yscale('log')
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(True, which="both")
        plt.savefig("New_" + reference + "/" + "Roc_n={}_m={}.png".format(n,m), format='png', bbox_inches='tight')
        plt.close()

        sig.append(eff_sig)
        bkg.append(rej_bkg)
        tuple_entry.append((n,m))

        if m == "All":
            MLPL_bkg = np.concatenate((cur_MLPL_jz0, cur_MLPL_jz1, cur_MLPL_jz2, cur_MLPL_jz3, cur_MLPL_jz4))
            MLPL_bkg_weights = np.concatenate((cur_MLPL_jz0_weights, cur_MLPL_jz1_weights, cur_MLPL_jz2_weights, cur_MLPL_jz3_weights, cur_MLPL_jz4_weights))
            mlplS, bins = np.histogram(MLPL_sig, bins=100, range=(-100,10), weights=MLPL_sig_weights, density=True)
            mlplB, bins = np.histogram(MLPL_bkg, bins=100, range=(-100,10), weights=MLPL_bkg_weights, density=True)

            plt.figure(figsize=(12,8))
            plt.step(bins[1:], mlplS, c='red', label="MLPL(4,All) Signal")
            plt.step(bins[1:], mlplB, c='blue', label="MLPL(4,All) Background")
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            plt.xlabel("MLPL(4, All)", fontsize=20)
            plt.ylabel("Number of Events", fontsize=20)
            plt.legend(fontsize=20, frameon=False)
            plt.savefig("New_" + reference + "/" + "MLPL.png", format='png', bbox_inches='tight')
            plt.close()


x_ref = np.linspace(0.01, 1, 10000)
color = iter(plt.cm.rainbow(np.linspace(0, 1, len(tuple_entry))))

plt.figure(figsize=(12,8))
for l, entry in enumerate(tuple_entry):
    c = next(color)
    plt.plot(sig[l], bkg[l], marker='o', color=c, label="MLPL{}".format(entry))
plt.plot(x_ref, 1/x_ref, linestyle='--', color='black', label=r"$\epsilon_{\mathrm{sig}}= \epsilon_{\mathrm{bkg}}$")
plt.legend(fontsize=20, frameon=False)
plt.title("ROC Curves", fontsize=20)
plt.xlabel(r"Signal Efficiency $\epsilon_{\mathrm{sig}}$", fontsize=20)
plt.ylabel(r"Background Rejection $1/\epsilon_{\mathrm{bkg}}$", fontsize=20)
plt.xlim(0,1)
plt.yscale('log')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.grid(True, which="both")
plt.savefig("New_" + reference + "/" + "Roc_total.png" , format='png', bbox_inches='tight')
plt.close()


############################################################################
print("Plotting successful.")
