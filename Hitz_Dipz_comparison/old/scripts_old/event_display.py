############################################################################

# Required packages

import h5py
import numpy as np
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt
from matplotlib import colors

############################################################################


# DipZ or HitZ

networks = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20231124-T152506/ckpts/epoch=039-val_loss=-2.01384__test_ttbar.h5"
}
reference = "Dipz"
test_path = '/eos/user/b/backes/QT/preprocessing/Dipz_output/pp_output_test_ttbar.h5'

# networks = {
#     "Hitz" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_20231120-T102448/ckpts/epoch=064-val_loss=-1.68750__test_ttbar.h5"
# }
# reference = "Hitz"
# test_path = '/eos/user/b/backes/QT/preprocessing/Hitz_output/pp_output_test_ttbar.h5'


############################################################################


# Load test data

#test_path = '/eos/user/b/backes/QT/preprocessing/' + reference + '_output/pp_output_test_ttbar.h5'   #tbd
num_jets = 35000

logger.info("Load data")
with h5py.File(test_path, 'r') as test_f:
    jets = test_f['jets'][:num_jets]
    jet_z = jets['TruthJetPVz']
    pt_MeV = jets['pt']
    pt = pt_MeV/1000
    eta = jets['eta']
    eventNumber = jets['eventNumber']
    #print(test_f.keys())
    #print(jets.dtype.names)

for key, val in networks.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        # print(jets.dtype.names)
        jet_z_pred = jets["gaussian_regression_TruthJetPVz"].reshape((-1,1))
        jet_z_stddev = jets["gaussian_regression_TruthJetPVz_stddev"].reshape((-1,1))
        jet_z_hitz = np.append(jet_z_pred, jet_z_stddev, axis=1)


############################################################################


# General plot

jet_z_plot = HistogramPlot(
    bins=np.linspace(-200, 200, 41),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)
jet_z_plot.add(
    Histogram(
        jet_z.flatten(), label="True"
    )
)
jet_z_plot.add(
        Histogram(jet_z_hitz[:,0].flatten(), label=reference)
)
jet_z_plot.draw()
jet_z_plot.savefig("Event_" + reference + "/jet_z.png")


############################################################################


# 2D Plot

plt.figure(figsize=(12,8))
h = plt.hist2d(jet_z, jet_z_hitz[:,0], bins=30, norm=colors.LogNorm())
cb = plt.colorbar(h[3])
cb.ax.tick_params(labelsize=20)
plt.xlabel("True position $z_t$", fontsize=20)
plt.ylabel("Predicted position $z_p$", fontsize=20)
plt.xlim(-150,150)
plt.ylim(-150,150)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
x = [-150,150]
plt.plot(x,x,c='red')
plt.savefig("Event_" + reference + "/2D_jet_z.png", format='png', bbox_inches='tight')
plt.close()


############################################################################


# Some single events

sorted_events = eventNumber.argsort()


def Gauss(x, mu, sigma):
    return 1/np.sqrt(2*np.pi*sigma**2) * np.exp(-(x-mu)**2/(2*sigma**2))

i=2

while i<12:

    for ev_num in np.unique(eventNumber[sorted_events]):
        
        if eventNumber[eventNumber==ev_num].shape[0] == i and np.unique(jet_z[eventNumber==ev_num]).shape[0] > 1:

            color = iter(plt.cm.rainbow(np.linspace(0, 1, i)))

            plt.figure(figsize=(12,8))
            x = np.linspace(-150,150, 100000)
            for j in range(i):
                c = next(color)
                y = Gauss(x, jet_z_hitz[eventNumber==ev_num][j,0], jet_z_hitz[eventNumber==ev_num][j,1])
                plt.plot(x[y>0.0001], y[y>0.0001]/np.max(y),
                        label = "$z_t=${:0.3f}, $z_p=${:0.3f}, $\sigma_p=${:0.3f}".format(
                            jet_z[eventNumber==ev_num][j],jet_z_hitz[eventNumber==ev_num][j,0], jet_z_hitz[eventNumber==ev_num][j,1],2),
                        color=c)
                plt.vlines(jet_z[eventNumber==ev_num][j], 0, 1, color=c, ls = '--', lw=2-j*0.2)

            plt.xlabel("$z$ Position", fontsize=20)
            plt.ylabel("Single event distributions", fontsize=20)
            plt.ylim(0,1.8)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            first_legend = plt.legend(loc="upper center", fontsize=15, ncol=2, frameon=False)
            plt.gca().add_artist(first_legend)
            # line1, = plt.plot([], label="$z_t$", linestyle='--', linewidth=2, c='black')
            # line2, = plt.plot([], label="$G(z_p,\\sigma_p)$", linewidth=2, c='black')
            # plt.legend(handles=[line1, line2], loc=(0.35,0.6), fontsize=15, ncol=2, frameon=False)
            plt.savefig("Event_" + reference + "/{}_jet_event.png".format(i), format='png', bbox_inches='tight')
            plt.close()
            break
    i += 1
    






# x1=0
# x2=0
# x3=0
# x4=0
# x5=0
# x6=0
# x7=0
# x8=0
# x9=0
# x10=0


# for ev_num in np.unique(eventNumber[sorted_events]):
#     if eventNumber[eventNumber==ev_num].shape[0]==1:
#         x1+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==2:
#         x2+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==3:
#         x3+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==4:
#         x4+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==5:
#         x5+=1
#         # print(eventNumber[eventNumber==ev_num])
#     if eventNumber[eventNumber==ev_num].shape[0]==6:
#         x6+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==7:
#         x7+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==8:
#         x8+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==9:
#         x9+=1
#     if eventNumber[eventNumber==ev_num].shape[0]==10:
#         x10+=1

# print(x1/np.unique(eventNumber[sorted_events]).shape[0])
# print(x2/np.unique(eventNumber[sorted_events]).shape[0])
# print(x3/np.unique(eventNumber[sorted_events]).shape[0])
# print(x4/np.unique(eventNumber[sorted_events]).shape[0])
# print(x5/np.unique(eventNumber[sorted_events]).shape[0])
# print(x6/np.unique(eventNumber[sorted_events]).shape[0])
# print(x7/np.unique(eventNumber[sorted_events]).shape[0])
# print(x8/np.unique(eventNumber[sorted_events]).shape[0])
# print(x9/np.unique(eventNumber[sorted_events]).shape[0])
# print(x10/np.unique(eventNumber[sorted_events]).shape[0])
# print((x1+x2+x3+x4+x5+x6+x7+x8+x9+x10)/np.unique(eventNumber[sorted_events]).shape[0])


############################################################################
print("Plotting successful.")